<?php

/**
 * Plugin Name: WP GDPR
 * Description: Complete General Data Protection Regulation compliance toolkit plugin for WordPress.
 * Version: 1.5.3
 * Author URI: https://www.createit.pl
 * Author: WP GDPR
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


    /**
     * Register a custom menu page.
     */
    function wpdocs_register_my_custom_menu_page(){
        add_menu_page( 
            __( 'Contact DPO', 'textdomain' ),
            'Contact DPO',
            'manage_options',
            'contactdpo',
            'my_custom_menu_page',
            8
        ); 
    }
    add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
     
    /**
     * Display a custom menu page
     */
    function my_custom_menu_page(){
	echo '<div class="gdpr_menu">
    <h2 class="nav-tab-wrapper">
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr">Introduction </a>
       <a class="nav-tab" href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr-cookie">Cookie consent </a>
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/edit.php?post_type=ct_ugdpr_service">Services Manager </a>
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr-terms">Terms and Conditions </a>
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr-policy">Privacy Policy </a>
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr-forgotten">Right To Be Forgotten </a>
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr-dataaccess">Data Access </a>
       <a class="nav-tab nav-tab-active" href="https://1website.co.uk/demo/wp-admin/admin.php?page=contactdpo">Contact DPO </a>
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr-rectification">Data Rectification </a>
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr-breach">Data Breach </a>
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr-services">Services </a>
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr-pseudonymization">Pseudonymization </a>
       <a class="nav-tab " href="https://1website.co.uk/demo/wp-admin/admin.php?page=ct-ultimate-gdpr-plugins">Collect Data </a>
       </h2>
</div> 
<h2 class="">Contact DPO</h2>';
        echo do_shortcode( '[contact-form-7 id="28" title="Contact form 1"]' );
    }
/**
 * Class CT_Ultimate_GDPR
 *
 */
class CT_Ultimate_GDPR {

	/**
	 * Plugin text-domain
	 */
	const DOMAIN = 'ct-ultimate-gdpr';

	/**
	 * @var $this
	 */
	private static $instance;

	/**
	 * @var CT_Ultimate_GDPR_Controller_Admin
	 */
	private $admin_controller;
	/**
	 * @var array
	 */
	private $controllers;

	/**
	 * Singleton
	 *
	 * @return CT_Ultimate_GDPR
	 */
	public static function instance() {

		if ( ! self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;

	}

	/**
	 * CT_Ultimate_GDPR constructor.
	 */
	private function __construct() {

		$this->register_autoload();
		$this->include_helpers();
		$this->include_acf();

		add_action( 'init', array( $this, 'init' ) );
		add_action( 'init', array( $this, 'register_services' ) );
		//add_action( 'admin_init', array( $this, 'update_cookie_manager_posts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts_action' ) );
		add_action( 'wp', array( $this, 'controller_actions' ) );
		add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );
		add_action( 'ct_ultimate_gdpr_after_controllers_registered', array( $this, 'update_plugin_database' ) );

	}

	/**
	 * Run on init
	 */
	public function init() {

		$this->register_controllers();
		$this->register_shortcodes();

	}

	/**
	 * Include helpers
	 */
	private function include_helpers() {

		require_once __DIR__ . "/includes/helpers.php";

		if ( file_exists( __DIR__ . "/vendor/optimus-prime-plugin-update/load.php" ) ) {
			require_once __DIR__ . "/vendor/optimus-prime-plugin-update/load.php";
		}

	}

	private function include_acf() {
		include_once plugin_dir_path(__FILE__) . 'vendor/acf/acf-filters.php';
		include plugin_dir_path(__FILE__) . 'vendor/acf-fields.php';
	}

	/**
	 * Register autoloader
	 */
	private function register_autoload() {

		if ( version_compare( phpversion(), '5.6.0', '>=' ) && is_readable( __DIR__ . "/vendor/autoload.php" ) ) {
			require_once __DIR__ . "/vendor/autoload.php";
		} else {
			spl_autoload_register( array( $this, 'autoload' ) );
		}

	}

	/**
	 * Custom class loader
	 *
	 * @param $classname
	 */
	public function autoload( $classname ) {

		if ( 0 !== stripos( $classname, 'ct_ultimate_gdpr' ) ) {
			return;
		}

		$normalized = str_ireplace( 'ct_ultimate_gdpr_', '', $classname );
		$normalized = str_replace( '_', '-', strtolower( $normalized ) );
		$path       = __DIR__ . "/includes";
		$namespaces = array( 'controller', 'service', 'shortcode', 'model', 'update' );

		foreach ( $namespaces as $namespace ) {

			if ( 0 === strpos( $normalized, "$namespace-" ) ) {
				$path .= "/$namespace";
			}

		}

		require_once $path . "/$normalized.php";

	}

	/**
	 * Run all regitered controllers actions
	 */
	public function controller_actions() {

		/** @var CT_Ultimate_GDPR_Controller_Abstract $controller */
		foreach ( $this->controllers as $controller ) {

			if ( is_admin() ) {
				$controller->admin_action();
			} else {
				$controller->front_action();
			}

		}

	}

	/**
	 * Register all default and custom controllers
	 */
	private function register_controllers() {

		$this->admin_controller = new CT_Ultimate_GDPR_Controller_Admin();

		foreach (
			array(
				new CT_Ultimate_GDPR_Controller_Cookie(),
				new CT_Ultimate_GDPR_Controller_Terms(),
				new CT_Ultimate_GDPR_Controller_Policy(),
				new CT_Ultimate_GDPR_Controller_Forgotten(),
				new CT_Ultimate_GDPR_Controller_Data_Access(),
				new CT_Ultimate_GDPR_Controller_Breach(),
				new CT_Ultimate_GDPR_Controller_Rectification(),
				new CT_Ultimate_GDPR_Controller_Services(),
				new CT_Ultimate_GDPR_Controller_Pseudonymization(),
				new CT_Ultimate_GDPR_Controller_Plugins(),
			) as $controller
		) {

			/** @var CT_Ultimate_GDPR_Controller_Abstract $controller */
			$this->controllers[ $controller->get_id() ] = $controller->set_options( $this->admin_controller->get_options( $controller->get_id() ) );
			$controller->init();

		}

		$controllers = apply_filters( 'ct_ultimate_gdpr_controllers', array() );

		foreach ( $controllers as $controller ) {

			if ( $controller instanceof CT_Ultimate_GDPR_Controller_Interface ) {
				$this->controllers[ $controller->get_id() ] = $controller;
			}

		}

		do_action( 'ct_ultimate_gdpr_after_controllers_registered', $this->controllers );

	}

	/**
	 * Register all shortcodes
	 */
	private function register_shortcodes() {
		new CT_Ultimate_GDPR_Shortcode_Myaccount();
		new CT_Ultimate_GDPR_Shortcode_Terms_Accept();
		new CT_Ultimate_GDPR_Shortcode_Policy_Accept();
		new CT_Ultimate_GDPR_Shortcode_Privacy_Policy();
	}

	/**
	 *
	 */
//	private function include_acf() {
//		include_once plugin_dir_path(__FILE__) . 'vendor/acf/acf-filters.php';
//		include plugin_dir_path(__FILE__) . 'vendor/acf-fields.php';
//	}

	/**
	 * Add scripts
	 */
	public function wp_enqueue_scripts_action() {
		wp_enqueue_style( 'ct-ultimate-gdpr', ct_ultimate_gdpr_url( '/assets/css/style.css' ) );
		wp_enqueue_style( 'font-awesome', ct_ultimate_gdpr_url( '/assets/css/fonts/font-awesome/css/font-awesome.min.css' ) );
	}

	/**
	 * @return CT_Ultimate_GDPR_Controller_Admin
	 */
	public function get_admin_controller() {
		return $this->admin_controller;
	}

	/**
	 * Register all services
	 */
	public function register_services() {
		CT_Ultimate_GDPR_Model_Services::instance()->get_services();
	}

	/**
	 * Get a controller
	 *
	 * @param $id
	 *
	 * @return mixed|null
	 */
	public function get_controller_by_id( $id ) {
		return isset( $this->controllers[ $id ] ) ? $this->controllers[ $id ] : null;
	}

	/**
	 * Load plugin text domain
	 */
	public function load_textdomain() {

		$locale    = is_admin() && function_exists( 'get_user_locale' ) ? get_user_locale() : get_locale();
		$locale    = apply_filters( 'ct_ultimate_gdpr_locale', $locale );
		$lang_file = trailingslashit( WP_LANG_DIR ) . 'plugins/' . self::DOMAIN . "-$locale.mo";

		load_textdomain( self::DOMAIN, $lang_file );
		load_plugin_textdomain( self::DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );

	}

	/**
	 * Load user defined cookies to posts
	 */
	public function update_plugin_database() {
		$obj = new CT_Ultimate_GDPR_Update_Legacy_Options();
		$obj->run_updater();
	}

	/**
	 * Load default predefined services to posts
	 */
	public function update_cookie_manager_posts() {
		$obj = new CT_Ultimate_GDPR_Update_Legacy_Options();
		$obj->update_cookie_manager_posts();
	}

}

CT_Ultimate_GDPR::instance();