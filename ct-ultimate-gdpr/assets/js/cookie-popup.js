/**
 * cookie popup features
 * @var object ct_ultimate_gdpr_cookie - from wp_localize_script
 *
 * */
jQuery(document).ready(function ($) {

    // hide popup and show small gear icon if user already given consent
    if (ct_ultimate_gdpr_cookie.consent) {
        jQuery('#ct-ultimate-gdpr-cookie-popup').hide();
        jQuery('#ct-ultimate-gdpr-cookie-open').show();
    } else {
        jQuery('#ct-ultimate-gdpr-cookie-popup').show();
    }

    function setTempCookie() {
        if (!document.cookie || -1 === document.cookie.indexOf('ct-ultimate-gdpr-cookie')) {
            document.cookie = "ct-ultimate-gdpr-cookie=%7B%22consent_declined%22%3Afalse%2C%22consent_expire_time%22%3A1557935720%2C%22consent_level%22%3A2%7D; expires=Thu, 18 Dec 2050 12:00:00 UTC; path=/";
        }
    }

    function onAccept() {
        jQuery.post(ct_ultimate_gdpr_cookie.ajaxurl, {
            "action": "ct_ultimate_gdpr_cookie_consent_give",
            "level": 5 /* maximum level */
        });
        jQuery('#ct-ultimate-gdpr-cookie-popup').hide();
        jQuery('#ct-ultimate-gdpr-cookie-open').show();
        $('body').removeClass("ct-ultimate-gdpr-cookie-topPanel-padding ct-ultimate-gdpr-cookie-bottomPanel-padding");
        $('body').removeClass("ct-ultimate-gdpr-cookie-topPanel-padding");

        setTempCookie();

    }

    function onRead() {
        if (ct_ultimate_gdpr_cookie && ct_ultimate_gdpr_cookie.readurl) {
            window.location.href = ct_ultimate_gdpr_cookie.readurl;
            console.log('cookie');
        }
    }

    function onSave() {
        jQuery.post(ct_ultimate_gdpr_cookie.ajaxurl, {
            "action": "ct_ultimate_gdpr_cookie_consent_give",
            "level": $('input[name=radio-group]:checked', '#ct-ultimate-gdpr-cookie-modal-slider-form').val()
        }, function () {
            window.location.reload(true);
        });

        $('body').removeClass("ct-ultimate-gdpr-cookie-topPanel-padding ct-ultimate-gdpr-cookie-bottomPanel-padding");
    }

    $('#ct-ultimate-gdpr-cookie-accept').bind('click', onAccept);
    $('#ct-ultimate-gdpr-cookie-read-more').bind('click', onRead);
    $('.ct-ultimate-gdpr-cookie-modal-btn.save').bind('click', onSave);

    //MODAL
    $('#ct-ultimate-gdpr-cookie-open,#ct-ultimate-gdpr-cookie-change-settings').on('click', function (e) {
        var modalbody = $("body");
        var modal = $("#ct-ultimate-gdpr-cookie-modal");
        modal.show();
        modalbody.addClass("cookie-modal-open");
        $("html").addClass("cookie-modal-open");
        e.stopPropagation();
    });

    //Close modal on x button
    $('#ct-ultimate-gdpr-cookie-modal-close').on('click', function () {
        var modalbody = $("body");
        var modal = $("#ct-ultimate-gdpr-cookie-modal");
        modal.hide();
        modalbody.removeClass("cookie-modal-open");
        $("html").removeClass("cookie-modal-open");
    });

    //Close modal when clicking outside of modal area.
    $(document).on("click", function (e) {
        if (!(e.target.closest('#ct-ultimate-gdpr-cookie-change-settings, .ct-ultimate-gdpr-cookie-modal-content'))) {
            var modalbody = $("body");
            var modal = $("#ct-ultimate-gdpr-cookie-modal");
            modal.hide();
            modalbody.removeClass("cookie-modal-open");
            $("html").removeClass("cookie-modal-open");
        }

        e.stopPropagation();
    });

    //SVG
    jQuery('img.ct-svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');
    });


    $(window).on('load', function () {

        var selected = $('.ct-ultimate-gdpr-cookie-modal-slider-item--active');
        var checked = selected.find('input[name=radio-group]');
        var input_id = checked.attr('id');

        selected.find('path').css('fill', '#82aa3b');
        selected.prevUntil('#ct-ultimate-gdpr-cookie-modal-slider-item-block').addClass('ct-ultimate-gdpr-cookie-modal-slider-item--selected');
        checked.parent().prevUntil('#ct-ultimate-gdpr-cookie-modal-slider-item-block').find('path').css('fill', '#82aa3b');

        $('#ct-ultimate-gdpr-cookie-modal-slider-form').attr('class', 'ct-slider-' + input_id);
        $('.ct-ultimate-gdpr-cookie-modal-slider-info.' + input_id).css('display', 'block');

    });

    $('.ct-ultimate-gdpr-cookie-modal-slider').each(function () {

        var $btns = $('.ct-ultimate-gdpr-cookie-modal-slider-item').click(function () {

            var $input = $(this).find('input').attr('id');

            var $el = $('.' + $input).show();
            var form_class = $('#ct-ultimate-gdpr-cookie-modal-slider-form');
            var modalBody = $('div#ct-ultimate-gdpr-cookie-modal-body');

            form_class.attr('class', 'ct-slider-' + $input);
            $('.ct-ultimate-gdpr-cookie-modal-slider-wrap > div').not($el).hide();

            $btns.removeClass('ct-ultimate-gdpr-cookie-modal-slider-item--active');
            $(this).addClass('ct-ultimate-gdpr-cookie-modal-slider-item--active');

            $(this).prevUntil('#ct-ultimate-gdpr-cookie-modal-slider-item-block').find('path').css('fill', '#82aa3b');
            $(this).prevUntil('#ct-ultimate-gdpr-cookie-modal-slider-item-block').addClass('ct-ultimate-gdpr-cookie-modal-slider-item--selected');
            $(this).find('path').css('fill', '#82aa3b');
            $(this).nextAll().find('path').css('fill', '#595959');
            $(this).removeClass('ct-ultimate-gdpr-cookie-modal-slider-item--selected');
            $(this).nextAll().removeClass('ct-ultimate-gdpr-cookie-modal-slider-item--selected');

            if ( $(this).attr('id') === 'ct-ultimate-gdpr-cookie-modal-slider-item-block' ) {
                modalBody.addClass('ct-ultimate-gdpr-slider-block');
                modalBody.removeClass('ct-ultimate-gdpr-slider-not-block');
            } else {
                modalBody.removeClass('ct-ultimate-gdpr-slider-block');
                modalBody.addClass('ct-ultimate-gdpr-slider-not-block');
            }

        });

    });

    if ($("#ct-ultimate-gdpr-cookie-popup").hasClass("ct-ultimate-gdpr-cookie-topPanel")) {
        $('body').addClass("ct-ultimate-gdpr-cookie-topPanel-padding");
    }

    if ($("#ct-ultimate-gdpr-cookie-popup").hasClass("ct-ultimate-gdpr-cookie-bottomPanel")) {
        $('body').addClass("ct-ultimate-gdpr-cookie-bottomPanel-padding");
    }

    if ($("#ct-ultimate-gdpr-cookie-popup").hasClass("ct-ultimate-gdpr-cookie-topPanel ct-ultimate-gdpr-cookie-popup-modern")) {
        $('body').addClass("popup-modern-style");
    }

    if ($("#ct-ultimate-gdpr-cookie-popup").hasClass("ct-ultimate-gdpr-cookie-bottomPanel ct-ultimate-gdpr-cookie-popup-modern")) {
        $('body').addClass("popup-modern-style");
    }

});
