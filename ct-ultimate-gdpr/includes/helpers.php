<?php

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

/**
 * Locate view template in stylesheet or plugin directory.
 *
 * @param $name
 * @param bool $include
 * @param array $options
 *
 * @return string
 */
function ct_ultimate_gdpr_locate_template( $name, $include = true, $options = array() ) {

	// view options
	$options = apply_filters( 'ct_ultimate_gdpr_locate_template_options', $options, $name );

	$include_dir_path = rtrim( get_stylesheet_directory(), '/' ) . "/ct-ultimate-gdpr";
	$path_to_file     = rtrim( $include_dir_path, '/' ) . "/$name.php";

	if ( ! is_readable( $path_to_file ) ) {
		$include_dir_path = __DIR__ . "/views";
	}

	$include_dir_path = apply_filters( 'ct_ultimate_gdpr_locate_template_path', $include_dir_path, $name );
	$path_to_file     = rtrim( $include_dir_path, '/' ) . "/$name.php";

	if ( $include ) {
		include $path_to_file;
	}

	return $path_to_file;
}

/**
 * @param string $append
 *
 * @return string
 */
function ct_ultimate_gdpr_url( $append = '' ) {
	return plugins_url( $append, __DIR__ );
}

/**
 * @param string $append
 *
 * @return string
 */
function ct_ultimate_gdpr_path( $append = '' ) {
	$path = dirname( __DIR__ );
	if ( $append ) {
		$path .= "/" . ltrim( $append, '/' );
	}

	return $path;
}

/**
 * Get value from array helper
 *
 * @param $variable
 * @param $array
 * @param bool $default
 * @param bool $allow_empty
 *
 * @return bool|mixed
 */
function ct_ultimate_gdpr_get_value( $variable, $array, $default = false, $allow_empty = true ) {
	return is_array( $array ) && isset( $array[ $variable ] ) && ( $allow_empty || ! empty( $array[ $variable ] ) ) ? $array[ $variable ] : $default;
}

/**
 * Format date
 *
 * @param $time
 *
 * @return false|string
 */
function ct_ultimate_gdpr_date( $time ) {
	$format = apply_filters( 'ct_ultimate_gdpr_date_format', 'd M Y H:i:s' );

	return date( $format, $time );
}

/**
 * @param string $to
 * @param string $title
 * @param $service_id
 * @param string $salt
 * @param $url
 */
function ct_ultimate_gdpr_send_confirm_mail( $to, $title, $service_id, $salt, $url ) {
	$hash    = ct_ultimate_gdpr_hash( $to, $salt );
	$url     = add_query_arg(
		array(
			'e' => $to,
			'h' => $hash,
			'i' => $service_id,
		),
		esc_url( $url )
	);
	$message = sprintf( esc_html__( 'To confirm, please follow this link: %s', 'ct-ultimate-gdpr' ), $url );
	wp_mail( $to, $title, $message, '' );
}

/**
 * @param $email
 * @param $salt
 * @param $hash
 *
 * @return bool
 */
function ct_ultimate_gdpr_check_confirm_mail( $email, $salt, $hash ) {
	return ct_ultimate_gdpr_hash( $email, $salt ) === $hash;
}

/**
 * @param string $string
 * @param string $salt
 *
 * @return string
 */
function ct_ultimate_gdpr_hash( $string, $salt ) {
	return substr( sha1( $string . $salt ), 5, 10 );
}

/**
 * @param $path
 * @param bool $output
 * @param array $options view options
 *
 * @return string
 */
function ct_ultimate_gdpr_render_template( $path, $output = false, $options = array() ) {

	$options = apply_filters( 'ct_ultimate_gdpr_render_template_options', $options, $path );

	ob_start();
	include $path;
	$rendered = ob_get_clean();

	$rendered = CT_Ultimate_GDPR_Model_Placeholders::instance()->replace( $rendered );

	if ( $output ) {
		echo $rendered;
	}

	return $rendered;
}

/**
 * Returns the translated object ID(post_type or term) or original if missing
 *
 * @param $object_id integer|string|array The ID/s of the objects to check and return
 * @param string $type the object type: post, page, {custom post type name}, nav_menu, nav_menu_item, category, tag etc.
 *
 * @return string|array of object ids
 */
function ct_ultimate_gdpr_wpml_translate_id( $object_id, $type = 'page' ) {

	$current_language = apply_filters( 'wpml_current_language', null );

	if ( is_array( $object_id ) ) {

		// if array

		$translated_object_ids = array();

		foreach ( $object_id as $id ) {
			$translated_object_ids[] = apply_filters( 'wpml_object_id', $id, $type, true, $current_language );
		}

		return $translated_object_ids;

	} elseif ( is_string( $object_id ) ) {

		// if string

		// check if we have a comma separated ID string
		$is_comma_separated = strpos( $object_id, "," );

		if ( $is_comma_separated !== false ) {

			// explode the comma to create an array of IDs
			$object_id = explode( ',', $object_id );

			$translated_object_ids = array();

			foreach ( $object_id as $id ) {
				$translated_object_ids[] = apply_filters( 'wpml_object_id', $id, $type, true, $current_language );
			}

			// make sure the output is a comma separated string (the same way it came in!)
			return implode( ',', $translated_object_ids );

		} else {

			// if we don't find a comma in the string then this is a single ID
			return apply_filters( 'wpml_object_id', intval( $object_id ), $type, true, $current_language );

		}

	} else {

		// if int
		return apply_filters( 'wpml_object_id', $object_id, $type, true, $current_language );

	}
}

/**
 * Get all original, not translated posts
 *
 * @param array $args get_posts args
 *
 * @return array
 */
function ct_ultimate_gdpr_wpml_get_original_posts( $args ) {

	/** @var SitePress */
	global $sitepress;

	// set default language
	if ( is_object( $sitepress ) && method_exists( $sitepress, 'get_default_language' ) ) {

		$default_language = $sitepress->get_default_language();
		$current_language = $sitepress->get_current_language();

		if ( ! empty( $default_language ) ) {
			$sitepress->switch_lang( $default_language );
		}

		$args['suppress_filters'] = false;

	}

    // get posts of default language
    $posts = get_posts($args);

	// restore set language
	if (
		is_object( $sitepress ) &&
		method_exists( $sitepress, 'switch_lang' ) &&
		! empty( $current_language )
	) {
		$sitepress->switch_lang( $current_language ); //restore previous language
	}

	return $posts;
}

/**
 * @return array
 */
function ct_ultimate_gpdr_get_default_post_types() {
	return apply_filters( 'ct_ultimate_gdpr_get_default_post_types', array( 'page' ) );
}

/**
 * @param $name
 * @param $value
 * @param $expire_time
 * @param string $path
 */
function ct_ultimate_gdpr_set_encoded_cookie( $name, $value, $expire_time, $path = '/' ) {
	$value = base64_encode( $value );
	setcookie( $name, $value, $expire_time, $path );
}

/**
 * @param $name
 *
 * @return bool|string
 */
function ct_ultimate_gdpr_get_encoded_cookie( $name ) {
	$value = ct_ultimate_gdpr_get_value( $name, $_COOKIE );
	$decoded = base64_decode( $value );

	return $decoded ? $decoded : $value;
}