<?php

/**
 * The template for displaying main plugin options page
 *
 * You can overwrite this template by copying it to yourtheme/ct-ultimate-gdpr/admin folder
 *
 * @version 1.0
 *
 */

?>

<div class="ct-ultimate-gdpr-wrap">

<?php $admin_url= admin_url(); ?>
<div class="gdpr_menu">
    <h2 class="nav-tab-wrapper">
       <a class="nav-tab nav-tab-active" href="<?php echo $admin_url.'admin.php?page=wp-gdpr'; ?>">Introduction </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-cookie'; ?>">Cookie consent </a>
       <a class="nav-tab " href="<?php echo $admin_url.'edit.php?post_type=ct_ugdpr_service'; ?>">Services Manager </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-terms'; ?>">Terms and Conditions </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-policy'; ?>">Privacy Policy </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-forgotten'; ?>">Right To Be Forgotten </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-dataaccess'; ?>">Data Access </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=contactdpo'; ?>">Contact DPO </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-rectification'; ?>">Data Rectification </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-breach'; ?>">Data Breach </a>
       <a class="nav-tab" href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-services'; ?>">Services </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-pseudonymization'; ?>">Pseudonymization </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-plugins'; ?>">Collect Data </a>
       </h2>
</div>


    <h3><?php echo esc_html__( 'Introduction', 'ct-ultimate-gdpr' ); ?></h3>

    <p>
		<?php echo esc_html__( "The GDPR was approved and adopted by the EU Parliament in April 2016. The regulation will take effect after a two-year transition period and, unlike a Directive it does not require any enabling legislation to be passed by government; meaning it will be in force May 2018.", 'ct-ultimate-gdpr' ); ?>
    </p>

    <p>
		<?php echo esc_html__( "The GDPR not only applies to organisations located within the EU but it will also apply to organisations located outside of the EU if they offer goods or services to, or monitor the behaviour of, EU data subjects. It applies to all companies processing and holding the personal data of data subjects residing in the European Union, regardless of the company's location.", 'ct-ultimate-gdpr' ); ?>
    </p>

    <p>

		<?php echo esc_html__( "This plugin will create a form where users can request access to or deletion of their personal data, stored on
        your website. It is also possible to:", 'ct-ultimate-gdpr' ); ?>

    </p>
    <ol>
        <li><?php echo esc_html__( "create a custom cookie notice and block all cookies until cookie consent is given", 'ct-ultimate-gdpr' ); ?></li>
        <li><?php echo esc_html__( "set up redirects for your Terms and Conditions and Privacy Policy pages until consent is given", 'ct-ultimate-gdpr' ); ?></li>
        <li><?php echo esc_html__( "browse user requests for data access/deletion and set custom email notifications", 'ct-ultimate-gdpr' ); ?></li>
        <li><?php echo esc_html__( "send custom email informing about data breach to all users which left their email at your site", 'ct-ultimate-gdpr' ); ?></li>
        <li><?php echo esc_html__( "automatically add consent boxes for various forms on your website", 'ct-ultimate-gdpr' ); ?></li>
        <li><?php echo esc_html__( "pseudonymize some of user data stored in database", 'ct-ultimate-gdpr' ); ?></li>
        <li><?php echo esc_html__( "check currently activated plugins for GDPR compliance", 'ct-ultimate-gdpr' ); ?></li>
    </ol>
    <p></p>

    <p>
		<?php echo esc_html__( "To start, browse through settings. Then, create a new page with shortcode: [wp_gdpr_myaccount]", 'ct-ultimate-gdpr' ); ?>
    </p>

</div>
