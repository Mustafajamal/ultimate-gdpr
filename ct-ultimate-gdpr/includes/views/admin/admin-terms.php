<?php

/**
 * The template for displaying terms controller view in wp-admin
 *
 * You can overwrite this template by copying it to yourtheme/ct-ultimate-gdpr/admin folder
 *
 * @version 1.0
 *
 */

?>

<?php $admin_url= admin_url(); ?>
<div class="gdpr_menu">
    <h2 class="nav-tab-wrapper">
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=wp-gdpr'; ?>">Introduction </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-cookie'; ?>">Cookie consent </a>
       <a class="nav-tab " href="<?php echo $admin_url.'edit.php?post_type=ct_ugdpr_service'; ?>">Services Manager </a>
       <a class="nav-tab nav-tab-active" href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-terms'; ?>">Terms and Conditions </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-policy'; ?>">Privacy Policy </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-forgotten'; ?>">Right To Be Forgotten </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-dataaccess'; ?>">Data Access </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=contactdpo'; ?>">Contact DPO </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-rectification'; ?>">Data Rectification </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-breach'; ?>">Data Breach </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-services'; ?>">Services </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-pseudonymization'; ?>">Pseudonymization </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-plugins'; ?>">Collect Data </a>
       </h2>
</div>

	<form method="post" action="options.php">

		<?php

		// This prints out all hidden setting fields
		settings_fields( CT_Ultimate_GDPR_Controller_Terms::ID );
		do_settings_sections( CT_Ultimate_GDPR_Controller_Terms::ID );
		submit_button();

		?>

	</form>
</div>

<form method="post">
    <input type="submit" class="button button-secondary" name="ct-ultimate-gdpr-log"
           value="<?php echo esc_html__( 'Download consents log', 'ct-ultimate-gdpr' ); ?>"/>
</form>
