<?php

/**
 * The template for displaying plugins controller view in wp-admin
 *
 * You can overwrite this template by copying it to yourtheme/ct-ultimate-gdpr/admin folder
 *
 * @version 1.0
 *
 */

?>

<div class="ct-ultimate-gdpr-wrap">
<?php $admin_url= admin_url(); ?>
<div class="gdpr_menu">
    <h2 class="nav-tab-wrapper">
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=wp-gdpr'; ?>">Introduction </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-cookie'; ?>">Cookie consent </a>
       <a class="nav-tab " href="<?php echo $admin_url.'edit.php?post_type=ct_ugdpr_service'; ?>">Services Manager </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-terms'; ?>">Terms and Conditions </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-policy'; ?>">Privacy Policy </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-forgotten'; ?>">Right To Be Forgotten </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-dataaccess'; ?>">Data Access </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=contactdpo'; ?>">Contact DPO </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-rectification'; ?>">Data Rectification </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-breach'; ?>">Data Breach </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-services'; ?>">Services </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-pseudonymization'; ?>">Pseudonymization </a>
       <a class="nav-tab nav-tab-active" href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-plugins'; ?>">Collect Data </a>
       </h2>
</div>

    <table class="wp-list-table widefat fixed striped">
        <thead>
        <tr>
            <th><?php echo esc_html__( 'Plugin', 'ct-ultimate-gdpr' ); ?></th>
            <th><?php echo esc_html__( 'Collects user data', 'ct-ultimate-gdpr' ); ?></th>
            <th><?php echo esc_html__( 'Compatible with WP GDPR', 'ct-ultimate-gdpr' ); ?></th>
        </tr>
        </thead>
        <tbody>

		<?php

		if ( ! empty( $options['plugins'] ) ) :

			foreach ( $options['plugins'] as $plugin ) : ?>

				<?php

				$row_style = '';

				if (
					$plugin['compatible'] === CT_Ultimate_GDPR_Controller_Plugins::PLUGIN_COMPATIBLE_YES ||
					$plugin['collects_data'] === CT_Ultimate_GDPR_Controller_Plugins::PLUGIN_COLLECTS_DATA_NO
				) :

					$row_style = 'style="background-color:lightgreen;"';

                elseif ( $plugin['compatible'] === CT_Ultimate_GDPR_Controller_Plugins::PLUGIN_COMPATIBLE_PARTLY ) :

					$row_style = 'style="background-color:yellow;"';

                elseif (
					$plugin['collects_data'] === CT_Ultimate_GDPR_Controller_Plugins::PLUGIN_COLLECTS_DATA_YES ||
					$plugin['collects_data'] === CT_Ultimate_GDPR_Controller_Plugins::PLUGIN_COLLECTS_DATA_PROBABLY
				) :

					$row_style = 'style="background-color:lightsalmon;"';

				else:

					$row_style = 'style="background-color:lightyellow;"';

				endif;

				?>

                <tr <?php echo $row_style; ?>>

                    <td><?php echo esc_html( $plugin['name'] ); ?></td>

                    <td>

						<?php

						if ( $plugin['collects_data'] === CT_Ultimate_GDPR_Controller_Plugins::PLUGIN_COLLECTS_DATA_YES ) {
							echo esc_html__( 'Yes', 'ct-ultimate-gdpr' );
						} elseif ( $plugin['collects_data'] === CT_Ultimate_GDPR_Controller_Plugins::PLUGIN_COLLECTS_DATA_NO ) {
							echo esc_html__( 'No', 'ct-ultimate-gdpr' );
						} elseif ( $plugin['collects_data'] === CT_Ultimate_GDPR_Controller_Plugins::PLUGIN_COLLECTS_DATA_PROBABLY ) {
							echo esc_html__( 'Probably', 'ct-ultimate-gdpr' );
						} else {
							echo esc_html__( 'Unknown', 'ct-ultimate-gdpr' );
						}

						?>

                    </td>

                    <td>

						<?php

						if ( $plugin['compatible'] === CT_Ultimate_GDPR_Controller_Plugins::PLUGIN_COMPATIBLE_YES ) {
							echo esc_html__( 'Yes', 'ct-ultimate-gdpr' );
						} elseif ( $plugin['compatible'] === CT_Ultimate_GDPR_Controller_Plugins::PLUGIN_COMPATIBLE_PARTLY ) {
							echo esc_html__( 'Partly', 'ct-ultimate-gdpr' );
						} else {
							echo esc_html__( 'No', 'ct-ultimate-gdpr' );
						}

						?>

                    </td>

                </tr>

			<?php

			endforeach;
		endif;

		?>

        </tbody>
        <tfoot>
        </tfoot>
    </table>

</div>