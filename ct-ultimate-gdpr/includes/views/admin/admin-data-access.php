<?php

/**
 * The template for displaying data access controller view in wp-admin
 *
 * You can overwrite this template by copying it to yourtheme/ct-ultimate-gdpr/admin folder
 *
 * @version 1.0
 *
 */

/** @var array $options */

?>

<?php if ( isset( $options['notices'] ) ) : ?>
	<?php foreach ( $options['notices'] as $notice ) : ?>

        <div class="ct-ultimate-gdpr notice-info notice">
			<?php echo esc_html( $notice ); ?>
        </div>

	<?php endforeach; endif; ?>

<div class="ct-ultimate-gdpr-wrap">
<?php $admin_url= admin_url(); ?>
<div class="gdpr_menu">
    <h2 class="nav-tab-wrapper">
       <a class="nav-tab" href="<?php echo $admin_url.'admin.php?page=wp-gdpr'; ?>">Introduction </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-cookie'; ?>">Cookie consent </a>
       <a class="nav-tab " href="<?php echo $admin_url.'edit.php?post_type=ct_ugdpr_service'; ?>">Services Manager </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-terms'; ?>">Terms and Conditions </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-policy'; ?>">Privacy Policy </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-forgotten'; ?>">Right To Be Forgotten </a>
       <a class="nav-tab nav-tab-active" href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-dataaccess'; ?>">Data Access </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=contactdpo'; ?>">Contact DPO </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-rectification'; ?>">Data Rectification </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-breach'; ?>">Data Breach </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-services'; ?>">Services </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-pseudonymization'; ?>">Pseudonymization </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-ultimate-gdpr-plugins'; ?>">Collect Data </a>
       </h2>
</div>

    <form method="post" action="options.php">

		<?php

		// This prints out all hidden setting fields
		settings_fields( CT_Ultimate_GDPR_Controller_Data_Access::ID );
		do_settings_sections( CT_Ultimate_GDPR_Controller_Data_Access::ID );
		submit_button();

		?>

    </form>
</div>

<h3><?php echo esc_html__( "Data access requests list", 'ct-ultimate-gdpr' ); ?></h3>

<table class="wp-list-table widefat fixed striped">
    <thead>
    <tr>
        <th><?php echo esc_html__( 'User ID', 'ct-ultimate-gdpr' ); ?></th>
        <th><?php echo esc_html__( 'Email', 'ct-ultimate-gdpr' ); ?></th>
        <th><?php echo esc_html__( 'Date of request', 'ct-ultimate-gdpr' ); ?></th>
        <th><?php echo esc_html__( 'Date of data sent', 'ct-ultimate-gdpr' ); ?></th>
        <th><?php echo esc_html__( 'Select for action', 'ct-ultimate-gdpr' ); ?></th>
        <th><?php echo esc_html__( 'See details', 'ct-ultimate-gdpr' ); ?></th>
    </tr>
    </thead>
    <tbody>

	<?php

	if ( ! empty( $options['data'] ) ) :

		foreach ( $options['data'] as $datum ) : ?>

            <tr>

                <td><?php echo $datum['id'] ? esc_html( $datum['id'] ) : esc_html__( 'Not registered', 'ct-ultimate-gdpr' ); ?></td>
                <td><?php echo esc_html( $datum['user_email'] ); ?></td>
                <td><?php echo esc_html( $datum['date'] ); ?></td>
                <td><?php echo $datum['status'] ? esc_html( $datum['status'] ) : esc_html__( 'Not sent', 'ct-ultimate-gdpr' ); ?></td>

                <td><input type="checkbox" form="ct-ultimate-gdpr-data-access-form" name="emails[]"
                           value="<?php echo esc_html( $datum['user_email'] ); ?>"/></td>
                <td>
                    <form method="post">
                        <input type="hidden" name="email" value="<?php echo esc_html( $datum['user_email'] ); ?>"/>
                        <input type="submit" class="button button-primary" name="ct-ultimate-gdpr-data-access-details"
                               value="<?php echo esc_html__( 'Preview data', 'ct-ultimate-gdpr' ); ?>"/>
                    </form>
                </td>

            </tr>

		<?php

		endforeach;
	endif;

	?>

    </tbody>
    <tfoot>
    <tr>
        <td colspan="6">
            <form method="post" id="ct-ultimate-gdpr-data-access-form">
                <input type="submit" class="button button-primary" name="ct-ultimate-gdpr-data-access-send"
                       value="<?php echo esc_html__( 'Send data to selected emails', 'ct-ultimate-gdpr' ); ?>">
                <input type="submit" class="button button-secondary" name="ct-ultimate-gdpr-data-access-remove"
                       value="<?php echo esc_html__( 'Remove selected requests from list', 'ct-ultimate-gdpr' ); ?>">
                <input type="submit" class="button button-secondary" name="ct-ultimate-gdpr-data-access-remove-all-sent"
                       value="<?php echo esc_html__( 'Remove all sent requests from list', 'ct-ultimate-gdpr' ); ?>">
            </form>
        </td>
    </tr>
    </tfoot>
</table>