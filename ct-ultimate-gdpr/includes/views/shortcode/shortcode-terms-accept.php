<?php

/**
 * The template for displaying [wp_gdpr_terms_accept] shortcode view in wp-admin
 *
 * You can overwrite this template by copying it to yourtheme/ct-ultimate-gdpr/shortcode folder
 *
 * @version 1.0
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/** @var array $options */

?>

<div class="ct-ultimate-gdpr-container container">

	<?php if ( ! empty( $options['terms_accepted'] ) ) : ?>

        <div id="ct-ultimate-gdpr-terms-accepted">
			<?php echo esc_html__( 'You have already accepted Terms and Conditions', 'ct-ultimate-gdpr' ); ?>
        </div>

        <button id="ct-ultimate-gdpr-terms-decline" class="ct-ultimate-gdpr-button">
			<?php echo esc_html__( 'Decline', 'ct-ultimate-gdpr' ); ?>
        </button>

	<?php else: ?>

        <button id="ct-ultimate-gdpr-terms-accept" class="ct-ultimate-gdpr-button">
			<?php echo esc_html__( 'Accept', 'ct-ultimate-gdpr' ); ?>
        </button>

	<?php endif; ?>

</div>