<?php

/**
 * Class CT_Ultimate_GDPR_Service_Mailster
 */
class CT_Ultimate_GDPR_Service_Mailster extends CT_Ultimate_GDPR_Service_Abstract {

	/**
	 * @return void
	 */
	public function init() {

		add_filter( 'ct_ultimate_gdpr_controller_plugins_compatible_mailster/mailster.php', '__return_true' );
		add_filter( 'ct_ultimate_gdpr_controller_plugins_collects_data_mailster/mailster.php', '__return_true' );
		add_filter( 'mailster_submit', array( $this, 'validate_consent' ), 10, 2 );

		// this happens also in ajax
		add_filter( 'mailster_form_fields', array( $this, 'add_form_fields' ), 10, 3 );
		add_filter( 'mymail_unsubscribe_form', array( $this, 'add_form_fields_unsubscribe' ), 10, 2 );

	}

	/**
	 * @return $this
	 */
	public function collect() {

		global $wpdb;

		$results = $wpdb->get_results(
			$wpdb->prepare( "
				SELECT s.*
				FROM {$wpdb->prefix}mailster_subscribers as s
				WHERE s.email = %s OR s.ID = %d		
				",
				$this->user->get_email(),
				$this->user->get_id()
			),
			ARRAY_A
		);

		foreach ( $results as $result ) {

			$meta_results = $wpdb->get_results(
				$wpdb->prepare( "
				SELECT sm.meta_key as sm_key, sm.meta_value as sm_value 
				FROM {$wpdb->prefix}mailster_subscriber_meta as sm
				WHERE sm.subscriber_id = %d		
				",
					$result['ID']
				),
				ARRAY_A
			);

			$results[] = $meta_results;

			$meta_results = $wpdb->get_results(
				$wpdb->prepare( "
				SELECT sf.meta_key as sf_key, sf.meta_value as sf_value 
				FROM {$wpdb->prefix}mailster_subscriber_fields as sm
				WHERE sf.subscriber_id = %d		
				",
					$result['ID']
				),
				ARRAY_A
			);

			$results[] = $meta_results;

		}

		$this->set_collected( $results );

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function get_name() {
		return 'Mailster';
	}

	/**
	 * @return bool
	 */
	public function is_active() {
		return function_exists( 'mailster' );
	}

	/**
	 * @return bool
	 */
	public function is_forgettable() {
		return true;
	}

	/**
	 * @throws Exception
	 * @return void
	 */
	public function forget() {

		global $wpdb;

		$result = $wpdb->query(
			$wpdb->prepare( "
				DELETE s, sf, sm 
				FROM {$wpdb->prefix}mailster_subscribers as s
				LEFT JOIN {$wpdb->prefix}mailster_subscriber_meta as sm
					ON s.ID = sm.subscriber_id
				LEFT JOIN {$wpdb->prefix}mailster_subscriber_fields as sf
					ON s.ID = sf.subscriber_id
				WHERE s.email = %s OR s.ID = %d		
				",
				$this->user->get_email(),
				$this->user->get_id()
			)
		);

		if ( false === $result ) {
			throw new Exception( sprintf( esc_html__( 'There were problems forgetting data for user: %s', 'ct-ultimate-gdpr' ), $this->user->get_email() ) );
		}

	}

	/**
	 * @return mixed
	 */
	public function add_option_fields() {

		add_settings_field(
			"services_{$this->get_id()}_header", // ID
			$this->get_name(), // Title
			'__return_empty_string', // Callback
			CT_Ultimate_GDPR_Controller_Services::ID, // Page
			CT_Ultimate_GDPR_Controller_Services::ID // Section
		);

		add_settings_field(
			"services_{$this->get_id()}_description", // ID
			sprintf( esc_html__( "[%s] Description", 'ct-ultimate-gdpr' ), $this->get_name() ), // Title
			array( $this, "render_description_field" ), // Callback
			CT_Ultimate_GDPR_Controller_Services::ID, // Page
			CT_Ultimate_GDPR_Controller_Services::ID // Section
		);

		add_settings_field(
			"services_{$this->get_id()}_consent_field", // ID
			esc_html__( "[{$this->get_name()}] Inject consent checkbox to all forms", 'ct-ultimate-gdpr' ), // Title
			array( $this, "render_field_services_{$this->get_id()}_consent_field" ), // Callback
			CT_Ultimate_GDPR_Controller_Services::ID, // Page
			CT_Ultimate_GDPR_Controller_Services::ID // Section
		);

		add_settings_field(
			"services_{$this->get_id()}_consent_field_position_first", // ID
			esc_html__( "[{$this->get_name()}] Inject consent checkbox as the first field instead of the last", 'ct-ultimate-gdpr' ), // Title
			array( $this, "render_field_services_{$this->get_id()}_consent_field_position_first" ), // Callback
			CT_Ultimate_GDPR_Controller_Services::ID, // Page
			CT_Ultimate_GDPR_Controller_Services::ID // Section
		);

		add_settings_field(
			'breach_services_mailster',
			esc_html__( 'Mailster', 'ct-ultimate-gdpr' ),
			array( $this, 'render_field_breach_services' ),
			CT_Ultimate_GDPR_Controller_Breach::ID,
			CT_Ultimate_GDPR_Controller_Breach::ID
		);

	}

	/**
	 *
	 */
	public function render_field_breach_services() {

		$admin      = CT_Ultimate_GDPR::instance()->get_admin_controller();
		$field_name = $admin->get_field_name( __FUNCTION__ );
		$values     = $admin->get_option_value( $field_name, array() );
		$checked    = in_array( $this->get_id(), $values ) ? 'checked' : '';
		printf(
			"<input class='ct-ultimate-gdpr-field' type='checkbox' id='%s' name='%s[]' value='%s' %s />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$this->get_id(),
			$checked
		);

	}

	/**
	 *
	 */
	public function render_field_services_mailster_consent_field() {

		$admin = CT_Ultimate_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input class='ct-ultimate-gdpr-field' type='checkbox' id='%s' name='%s' %s />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name ) ? 'checked' : ''
		);

	}

	/**
	 *
	 */
	public function render_field_services_mailster_consent_field_position_first() {

		$admin = CT_Ultimate_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input class='ct-ultimate-gdpr-field' type='checkbox' id='%s' name='%s' %s />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name ) ? 'checked' : ''
		);

	}

	/**
	 * @return mixed
	 */
	public function front_action() {
	}

	/**
	 * @param $html
	 * @param $campaign_id
	 *
	 * @return mixed
	 */
	public function add_form_fields_unsubscribe( $html, $campaign_id ) {

		$original = $html;

		$inject = CT_Ultimate_GDPR::instance()->get_admin_controller()->get_option_value( "services_{$this->get_id()}_consent_field", false, CT_Ultimate_GDPR_Controller_Services::ID );

		// option set not to inject a checkbox
		if ( ! $inject ) {
			return $html;
		}

		$content = ct_ultimate_gdpr_render_template( ct_ultimate_gdpr_locate_template( 'service/service-mailster-consent-field', false ) ) . '</form>';
		$html    = str_replace( '</form>', $content, $html );

		return apply_filters( "ct_ultimate_gdpr_service_{$this->get_id()}_form_unsubscribe_content", $html, $inject, $original );

	}

	/**
	 *
	 * @param $fields
	 * @param $form_id
	 * @param $form
	 *
	 * @return mixed
	 */
	public function add_form_fields( $fields, $form_id, $form ) {

		$position_first = CT_Ultimate_GDPR::instance()->get_admin_controller()->get_option_value( "services_{$this->get_id()}_consent_field_position_first", false, CT_Ultimate_GDPR_Controller_Services::ID );
		$inject         = CT_Ultimate_GDPR::instance()->get_admin_controller()->get_option_value( "services_{$this->get_id()}_consent_field", false, CT_Ultimate_GDPR_Controller_Services::ID );

		// option set not to inject a checkbox
		if ( ! $inject ) {
			return $fields;
		}

		if ( $position_first ) {

			$fields = array_merge( array( 'ct_consent' => ct_ultimate_gdpr_render_template( ct_ultimate_gdpr_locate_template( 'service/service-mailster-consent-field', false ) ) ), $fields );

		} else {

			$position = count( $fields );

			foreach ( $fields as $key => $field ) {

				if ( stripos( $key, 'submit' ) !== false ) {
					$position = array_search( $key, array_keys( $fields ) );
					break;
				}

			}

			$head   = array_splice( $fields, 0, $position );
			$fields = array_merge( $head, array( 'ct_consent' => ct_ultimate_gdpr_render_template( ct_ultimate_gdpr_locate_template( 'service/service-mailster-consent-field', false ) ) ), $fields );
		}

		return apply_filters( "ct_ultimate_gdpr_service_{$this->get_id()}_form_content", $fields, $inject, $position_first, $form_id, $form );
	}

	/**
	 * @param array $recipients
	 *
	 * @return array
	 */
	public function breach_recipients_filter( $recipients ) {
		return array_merge( $recipients, $this->get_all_users_emails() );
	}


	/**
	 * @return array
	 */
	private function get_all_users_emails() {

		global $wpdb;

		$results = $wpdb->get_results( "
				SELECT s.email
				FROM {$wpdb->prefix}mailster_subscribers as s		
				",
			ARRAY_A
		);

		$emails = array();

		foreach ( $results as $result ) {
			$emails[] = $result['email'];
		}

		return $emails;

	}


	/**
	 * @param array $form_data
	 *
	 * @return array
	 */
	public function validate_consent( $form_data ) {

		$inject = CT_Ultimate_GDPR::instance()->get_admin_controller()->get_option_value( "services_{$this->get_id()}_consent_field", false, CT_Ultimate_GDPR_Controller_Services::ID );

		if ( $inject && empty( $_POST['ct-ultimate-gdpr-consent-field'] ) ) {

			if ( empty( $form_data['errors'] ) ) {
				$form_data['errors'] = array();
			}

			$form_data['errors']['ct_consent'] = esc_html__( 'Consent is missing', 'ct-ultimate-gdpr' );

		}

		return $form_data;
	}

	/**
	 * @return string
	 */
	protected function get_default_description() {
		return esc_html__( 'Mailster collects signed in user data', 'ct-ultimate-gdpr' );
	}
}