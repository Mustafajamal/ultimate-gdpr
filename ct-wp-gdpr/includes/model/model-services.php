<?php

/**
 * Class CT_Wp_GDPR_Model_Services
 */
class CT_Wp_GDPR_Model_Services {

	/** @var array */
	private $services;

	/** @var self */
	private static $instance;

	/**
	 * @return CT_Wp_GDPR_Model_Services
	 */
	public static function instance() {

		if ( ! self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * CT_Wp_GDPR_Model_Services constructor.
	 */
	private function __construct() {
		$this->services = array();
	}

	/**
	 * @param array $options
	 *
	 * @param string $type
	 *
	 * @return array
	 */
	public function get_services( $options = array(), $type = 'all' ) {

		if ( ! $this->services ) {
			$this->load_services( $options );
		}

		if ( $type == 'all' ) {
			return $this->services;
		}

		$filtered = array();

		/** @var CT_Wp_GDPR_Service_Abstract $service */
		foreach ( $this->services as $service ) {

			$method = "is_$type";

			if ( method_exists( $service, $method ) && $service->$method() ) {

				$filtered[ $service->get_id() ] = $service;

			}

		}

		return $filtered;

	}

	/**
	 * @param $service
	 *
	 * @return $this
	 */
	private function add_service( $service ) {

		if ( $service instanceof CT_Wp_GDPR_Service_Interface ) {
			if ( $service->is_active() ) {
				$this->services[ $service->get_id() ] = $service;
			}
		}

		return $this;

	}

	/**
	 * Load all services
	 *
	 * @param array $options
	 */
	private function load_services( $options ) {

		/**
		 * Instantiated services will auto register.
		 */
		$default_services_classes = apply_filters( 'ct_wp_gdpr_model_services_default', array(
				'CT_Wp_GDPR_Service_Addthis',
				'CT_Wp_GDPR_Service_ARForms',
				'CT_Wp_GDPR_Service_Buddypress',
				'CT_Wp_GDPR_Service_Contact_Form_7',
				'CT_Wp_GDPR_Service_Events_Manager',
				'CT_Wp_GDPR_Service_Facebook_Pixel',
				'CT_Wp_GDPR_Service_Formidable_Forms',
				'CT_Wp_GDPR_Service_GA_Google_Analytics',
				'CT_Wp_GDPR_Service_Google_Analytics',
				'CT_Wp_GDPR_Service_Google_Analytics_Dashboard_For_WP',
				'CT_Wp_GDPR_Service_Google_Analytics_For_Wordpress',
				'CT_Wp_GDPR_Service_Gravity_Forms',
				'CT_Wp_GDPR_Service_Mailchimp',
				'CT_Wp_GDPR_Service_Mailster',
				'CT_Wp_GDPR_Service_Quform',
				'CT_Wp_GDPR_Service_Woocommerce',
				'CT_Wp_GDPR_Service_WP_Comments',
				'CT_Wp_GDPR_Service_WP_Posts',
				'CT_Wp_GDPR_Service_WP_User',
				'CT_Wp_GDPR_Service_Wp_Job_Manager',
				'CT_Wp_GDPR_Service_WP_Simple_Paypal_Shopping_Cart',
			)
		);

		foreach ( $default_services_classes as $default_service ) {
			new $default_service;
		}

		$all_services = apply_filters( 'ct_wp_gdpr_load_services', array(), $options, $this->services );

		foreach ( $all_services as $service ) {
			$this->add_service( $service );
		}
	}

	/**
	 * @param $service_id
	 *
	 * @return bool|CT_Wp_GDPR_Service_Abstract
	 */
	public function get_service_by_id( $service_id ) {
		$this->get_services();

		return isset( $this->services[ $service_id ] ) ? $this->services[ $service_id ] : false;
	}


}