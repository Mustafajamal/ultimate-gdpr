<?php

/**
 * Class CT_Wp_GDPR_Service_ARForms
 */
class CT_Wp_GDPR_Service_ARForms extends CT_Wp_GDPR_Service_Abstract {

	/**
	 * @return void
	 */
	public function init() {
		add_filter( 'ct_wp_gdpr_controller_plugins_compatible_arforms/arforms.php', '__return_true' );
		add_filter( 'ct_wp_gdpr_controller_plugins_collects_data_arforms/arforms.php', '__return_true' );

	}

	/**
	 * @return $this
	 */
	public function collect() {

		global $wpdb;

		$results = $wpdb->get_results(
			$wpdb->prepare( "
				SELECT e.id as eid, e.*
				FROM {$wpdb->prefix}arf_entry_values as ev
					INNER JOIN {$wpdb->prefix}arf_entries as e
					ON e.id = ev.entry_id
				WHERE ev.entry_value = %s
				",
				$this->user->get_email()
			),
			ARRAY_A
		);

		foreach ( $results as $key => $result ) {

			$meta_results = $wpdb->get_results(
				$wpdb->prepare( "
				SELECT ev.entry_value
				FROM {$wpdb->prefix}arf_entry_values as ev
				WHERE ev.entry_id = %d		
				",
					$result['eid']
				),
				ARRAY_A
			);

			$results[ $key ][] = $meta_results;

		}

		$this->set_collected( $results );

		return $this;

	}

	/**
	 * @return mixed
	 */
	public function get_name() {
		return 'ARForms';
	}

	/**
	 * @return bool
	 */
	public function is_active() {
		return class_exists( 'arformcontroller' );
	}

	/**
	 * @return bool
	 */
	public function is_forgettable() {
		return true && $this->is_active();
	}

	/**
	 * @throws Exception
	 * @return void
	 */
	public function forget() {

		global $wpdb;

		$this->collect();

		foreach ( $this->collected as $collected ) {

			$wpdb->delete(
				"{$wpdb->prefix}arf_entries",
				array( 'id' => $collected['eid'] )
			);

			$wpdb->delete(
				"{$wpdb->prefix}arf_entry_values",
				array( 'entry_id' => $collected['eid'] )
			);

		}
	}

	/**
	 * @return mixed
	 */
	public function add_option_fields() {

		add_settings_field(
			"services_{$this->get_id()}_header", // ID
			$this->get_name(), // Title
			'__return_empty_string', // Callback
			CT_Wp_GDPR_Controller_Services::ID, // Page
			CT_Wp_GDPR_Controller_Services::ID // Section
		);

		add_settings_field(
			"services_{$this->get_id()}_description", // ID
			esc_html__( "[ARForms] Description", 'ct-wp-gdpr' ), // Title
			array( $this, "render_description_field" ), // Callback
			CT_Wp_GDPR_Controller_Services::ID, // Page
			CT_Wp_GDPR_Controller_Services::ID // Section
		);

		add_settings_field(
			'services_arforms_consent_field', // ID
			esc_html__( '[ARForms] Inject consent checkbox to all forms', 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_services_arforms_consent_field' ), // Callback
			CT_Wp_GDPR_Controller_Services::ID, // Page
			CT_Wp_GDPR_Controller_Services::ID // Section
		);

		add_settings_field(
			'breach_services_arforms',
			esc_html__( 'ARForms', 'ct-wp-gdpr' ),
			array( $this, 'render_field_breach_services' ),
			CT_Wp_GDPR_Controller_Breach::ID,
			CT_Wp_GDPR_Controller_Breach::ID
		);

	}

	/**
	 *
	 */
	public function render_field_breach_services() {

		$admin      = CT_Wp_GDPR::instance()->get_admin_controller();
		$field_name = $admin->get_field_name( __FUNCTION__ );
		$values     = $admin->get_option_value( $field_name, array() );
		$checked    = in_array( $this->get_id(), $values ) ? 'checked' : '';
		printf(
			"<input class='ct-wp-gdpr-field' type='checkbox' id='%s' name='%s[]' value='%s' %s />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$this->get_id(),
			$checked
		);

	}

	/**
	 * @param array $recipients
	 *
	 * @return array
	 */
	public function breach_recipients_filter( $recipients ) {

		global $wpdb;

		$results = $wpdb->get_results("
				SELECT entry_value
				FROM {$wpdb->prefix}arf_entry_values
				WHERE entry_value REGEXP '^[A-Za-z0-9._%\-+!#$&/=?^|~]+@[A-Za-z0-9.-]+[.][A-Za-z]+$'
				",
			ARRAY_A
		);

		foreach ( $results as $result ) {

			if ( ! empty( $result['entry_value'] ) ) {
				$recipients[ $result['entry_value'] ] = $result['entry_value'];
			}

		}

		return $recipients;

	}

	/**
	 *
	 */
	public function render_field_services_arforms_consent_field() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input class='ct-wp-gdpr-field' type='checkbox' id='%s' name='%s' %s />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name ) ? 'checked' : ''
		);

	}

	/**
	 *
	 */
	public function render_field_services_arforms_consent_field_position_first() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input class='ct-wp-gdpr-field' type='checkbox' id='%s' name='%s' %s />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name ) ? 'checked' : ''
		);

	}

	/**
	 * @return mixed
	 */
	public function front_action() {
		add_filter( 'arfentryform', array( $this, 'add_consent_checkbox' ) );
	}

	public function add_consent_checkbox( $form ) {

		$inject = CT_Wp_GDPR::instance()->get_admin_controller()->get_option_value( 'services_arforms_consent_field', false, CT_Wp_GDPR_Controller_Services::ID );

		if ( $inject ) {
			$form .= ct_wp_gdpr_render_template( ct_wp_gdpr_locate_template( 'service/service-arforms-consent-field', false ), false );
		}

		return $form;
	}

	/**
	 * @return string
	 */
	protected function get_default_description() {
		return esc_html__( 'ARForms gathers data entered by users in forms', 'ct-wp-gdpr' );
	}

}
