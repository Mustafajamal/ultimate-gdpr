<?php

/**
 * The template for displaying main plugin options page
 *
 * You can overwrite this template by copying it to yourtheme/ct-wp-gdpr/admin folder
 *
 * @version 1.0
 *
 */

?>
<?php $admin_url= admin_url(); ?>
<div class="gdpr_menu">
    <h2 class="nav-tab-wrapper">
       <a class="nav-tab nav-tab-active" href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr'; ?>">Introduction </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-cookie'; ?>">Cookie consent </a>
       <a class="nav-tab " href="<?php echo $admin_url.'edit.php?post_type=ct_ugdpr_service'; ?>">Services Manager </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-terms'; ?>">Terms and Conditions </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-policy'; ?>">Privacy Policy </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-forgotten'; ?>">Right To Be Forgotten </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-dataaccess'; ?>">Data Access </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=contactdpo'; ?>">Contact DPO </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-rectification'; ?>">Data Rectification </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-breach'; ?>">Data Breach </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-services'; ?>">Services </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-pseudonymization'; ?>">Pseudonymization </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-plugins'; ?>">Collect Data </a>
       </h2>
</div>
<div class="ct-wp-gdpr-wrap">


    <h3><?php echo esc_html__( 'Introduction', 'ct-wp-gdpr' ); ?></h3>


    <p>
		<?php echo esc_html__( "The GDPR was approved and adopted by the EU Parliament in April 2016. The regulation will take effect after a two-year transition period and, unlike a Directive it does not require any enabling legislation to be passed by government; meaning it will be in force May 2018.", 'ct-wp-gdpr' ); ?>
    </p>

    <p>
		<?php echo esc_html__( "The GDPR not only applies to organisations located within the EU but it will also apply to organisations located outside of the EU if they offer goods or services to, or monitor the behaviour of, EU data subjects. It applies to all companies processing and holding the personal data of data subjects residing in the European Union, regardless of the company's location.", 'ct-wp-gdpr' ); ?>
    </p>

    <p>

		<?php echo esc_html__( "This plugin will create a form where users can request access to or deletion of their personal data, stored on
        your website. It is also possible to:", 'ct-wp-gdpr' ); ?>

    </p>
    <ol>
        <li><?php echo esc_html__( "create a custom cookie notice and block all cookies until cookie consent is given", 'ct-wp-gdpr' ); ?></li>
        <li><?php echo esc_html__( "set up redirects for your Terms and Conditions and Privacy Policy pages until consent is given", 'ct-wp-gdpr' ); ?></li>
        <li><?php echo esc_html__( "browse user requests for data access/deletion and set custom email notifications", 'ct-wp-gdpr' ); ?></li>
        <li><?php echo esc_html__( "send custom email informing about data breach to all users which left their email at your site", 'ct-wp-gdpr' ); ?></li>
        <li><?php echo esc_html__( "automatically add consent boxes for various forms on your website", 'ct-wp-gdpr' ); ?></li>
        <li><?php echo esc_html__( "pseudonymize some of user data stored in database", 'ct-wp-gdpr' ); ?></li>
        <li><?php echo esc_html__( "check currently activated plugins for GDPR compliance", 'ct-wp-gdpr' ); ?></li>
    </ol>
    <p></p>

    <p>
		<?php echo esc_html__( "To start, browse through settings. Then, create a new page with shortcode: [wp_gdpr_myaccount]", 'ct-wp-gdpr' ); ?>
    </p>

</div>
