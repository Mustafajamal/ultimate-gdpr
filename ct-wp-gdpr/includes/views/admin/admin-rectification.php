<?php

/**
 * The template for displaying rectification controller view in wp-admin
 *
 * You can overwrite this template by copying it to yourtheme/ct-wp-gdpr/admin folder
 *
 * @version 1.0
 *
 */

/** @var array $options */

?>
<?php $admin_url= admin_url(); ?>
<div class="gdpr_menu">
    <h2 class="nav-tab-wrapper">
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr'; ?>">Introduction </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-cookie'; ?>">Cookie consent </a>
       <a class="nav-tab " href="<?php echo $admin_url.'edit.php?post_type=ct_ugdpr_service'; ?>">Services Manager </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-terms'; ?>">Terms and Conditions </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-policy'; ?>">Privacy Policy </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-forgotten'; ?>">Right To Be Forgotten </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-dataaccess'; ?>">Data Access </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=contactdpo'; ?>">Contact DPO </a>
       <a class="nav-tab nav-tab-active" href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-rectification'; ?>">Data Rectification </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-breach'; ?>">Data Breach </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-services'; ?>">Services </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-pseudonymization'; ?>">Pseudonymization </a>
       <a class="nav-tab " href="<?php echo $admin_url.'admin.php?page=ct-wp-gdpr-plugins'; ?>">Collect Data </a>
       </h2>
</div>
<?php if ( isset( $options['notices'] ) ) : ?>
	<?php foreach ( $options['notices'] as $notice ) : ?>

        <div class="ct-wp-gdpr notice-info notice">
			<?php echo esc_html( $notice ); ?>
        </div>

	<?php endforeach; endif; ?>

<div class="ct-wp-gdpr-wrap">

    <form method="post" action="options.php">

		<?php

		// This prints out all hidden setting fields
		settings_fields( CT_Wp_GDPR_Controller_Rectification::ID );
		do_settings_sections( CT_Wp_GDPR_Controller_Rectification::ID );
		submit_button();

		?>

    </form>
</div>

<h3><?php echo esc_html__( "Data rectification requests list", 'ct-wp-gdpr' ); ?></h3>

<table class="wp-list-table widefat fixed striped">
    <thead>
    <tr>
        <th><?php echo esc_html__( 'User ID', 'ct-wp-gdpr' ); ?></th>
        <th><?php echo esc_html__( 'Email', 'ct-wp-gdpr' ); ?></th>
        <th><?php echo esc_html__( 'Date of request', 'ct-wp-gdpr' ); ?></th>
        <th><?php echo esc_html__( 'Date of data sent', 'ct-wp-gdpr' ); ?></th>
        <th><?php echo esc_html__( 'Select for action', 'ct-wp-gdpr' ); ?></th>
        <th><?php echo esc_html__( 'See details', 'ct-wp-gdpr' ); ?></th>
    </tr>
    </thead>
    <tbody>

	<?php

	if ( ! empty( $options['data'] ) ) :

		foreach ( $options['data'] as $datum ) : ?>

            <tr>

                <td><?php echo $datum['id'] ? esc_html( $datum['id'] ) : esc_html__( 'Not registered', 'ct-wp-gdpr' ); ?></td>
                <td><?php echo esc_html( $datum['user_email'] ); ?></td>
                <td><?php echo esc_html( $datum['date'] ); ?></td>
                <td><?php echo $datum['status'] ? esc_html( $datum['status'] ) : esc_html__( 'Not sent', 'ct-wp-gdpr' ); ?></td>

                <td><input type="checkbox" form="ct-wp-gdpr-rectification-form" name="emails[]"
                           value="<?php echo esc_html( $datum['user_email'] ); ?>"/></td>
                <td>
                    <form method="post">
                        <input type="hidden" name="email" value="<?php echo esc_html( $datum['user_email'] ); ?>"/>
                        <input type="submit" class="button button-primary" name="ct-wp-gdpr-rectification-details"
                               value="<?php echo esc_html__( 'See rectified data', 'ct-wp-gdpr' ); ?>"/>
                    </form>
                </td>

            </tr>

		<?php

		endforeach;
	endif;

	?>

    </tbody>
    <tfoot>
    <tr>
        <td colspan="6">
            <form method="post" id="ct-wp-gdpr-rectification-form">
                <input type="submit" class="button button-primary" name="ct-wp-gdpr-rectification-send"
                       value="<?php echo esc_html__( 'Send data to selected emails', 'ct-wp-gdpr' ); ?>">
                <input type="submit" class="button button-secondary" name="ct-wp-gdpr-rectification-remove"
                       value="<?php echo esc_html__( 'Remove selected requests from list', 'ct-wp-gdpr' ); ?>">
                <input type="submit" class="button button-secondary" name="ct-wp-gdpr-rectification-remove-all-sent"
                       value="<?php echo esc_html__( 'Remove all sent requests from list', 'ct-wp-gdpr' ); ?>">
            </form>
        </td>
    </tr>
    </tfoot>
</table>