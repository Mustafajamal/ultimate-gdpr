<?php

/**
 * The template for displaying Mailchimp service view in wp-admin
 *
 * You can overwrite this template by copying it to yourtheme/ct-wp-gdpr/service folder
 *
 * @version 1.0
 *
 */

?>

<input class="ct-wp-gdpr-consent-field" type="checkbox" name="ct-wp-gdpr-consent-field" required />
<label for="ct-wp-gdpr-consent-field">
	<?php echo esc_html__( 'I consent to the storage of my data according to the Privacy Policy ', 'ct-wp-gdpr' ); ?>
</label>