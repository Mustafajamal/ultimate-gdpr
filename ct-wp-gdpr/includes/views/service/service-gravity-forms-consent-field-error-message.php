<?php

/**
 * The template for displaying Gravity Forms service view in wp-admin
 *
 * You can overwrite this template by copying it to yourtheme/ct-wp-gdpr/service folder
 *
 * @version 1.0
 *
 */

echo esc_html__( 'You must accept the Privacy Policy before submitting the form', 'ct-wp-gdpr' );