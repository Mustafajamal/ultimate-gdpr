<?php

/**
 * The template for displaying Gravity Forms service view in wp-admin
 *
 * You can overwrite this template by copying it to yourtheme/ct-wp-gdpr/service folder
 *
 * @version 1.0
 *
 */

echo esc_html__( 'I consent to storage of my data according to Privacy Policy', 'ct-wp-gdpr' );