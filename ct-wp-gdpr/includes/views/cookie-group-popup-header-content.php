<?php

/**
 * The template for displaying cookie group popup on front
 *
 * You can overwrite this template by copying it to yourtheme/ct-wp-gdpr folder
 *
 * @version 1.0
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/** @var array $options */

?>

            <h2><?php esc_html_e( 'Privacy settings', 'ct-wp-gdpr' ); ?></h2>
            <div class="ct-wp-gdpr-cookie-modal-desc">
                <p><?php esc_html_e( 'Decide which cookies you want to allow.', 'ct-wp-gdpr' ); ?></p>
                <p><?php esc_html_e( 'You can change these settings at any time. However, this can result in some functions no longer being available. For information on deleting the cookies, please consult your browser’s help function.', 'ct-wp-gdpr' ); ?></p>
                <span><?php esc_html_e( 'Learn more about the cookies we use.', 'ct-wp-gdpr' ); ?></span>
            </div>
            <h3><?php esc_html_e( 'With the slider, you can enable or disable different types of cookies:', 'ct-wp-gdpr' ); ?></h3>
