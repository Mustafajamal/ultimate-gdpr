<?php

/**
 * The template for displaying [wp_gdpr_terms_accept] shortcode view in wp-admin
 *
 * You can overwrite this template by copying it to yourtheme/ct-wp-gdpr/shortcode folder
 *
 * @version 1.0
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/** @var array $options */

?>

<div class="ct-wp-gdpr-container container">

	<?php if ( ! empty( $options['terms_accepted'] ) ) : ?>

        <div id="ct-wp-gdpr-terms-accepted">
			<?php echo esc_html__( 'You have already accepted Terms and Conditions', 'ct-wp-gdpr' ); ?>
        </div>

        <button id="ct-wp-gdpr-terms-decline" class="ct-wp-gdpr-button">
			<?php echo esc_html__( 'Decline', 'ct-wp-gdpr' ); ?>
        </button>

	<?php else: ?>

        <button id="ct-wp-gdpr-terms-accept" class="ct-wp-gdpr-button">
			<?php echo esc_html__( 'Accept', 'ct-wp-gdpr' ); ?>
        </button>

	<?php endif; ?>

</div>