<?php

/**
 * Class CT_Wp_GDPR_Shortcode_Cookie_Popup
 */
class CT_Wp_GDPR_Shortcode_Cookie_Popup {

	/**
	 * @var string
	 */
	public static $tag = 'wp_gdpr_cookie_popup';

	/**
	 * CT_Wp_GDPR_Shortcode_Cookie_Popup constructor.
	 */
	public function __construct() {
		add_shortcode( self::$tag, array( $this, 'process' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts_action' ) );
	}

	public function wp_enqueue_scripts_action() {

		if ( get_post() && false !== strpos( get_post()->post_content, "[{$this::$tag}]" ) ) {

			wp_localize_script( 'ct-wp-gdpr-cookie-popup', 'ct_wp_gdpr_cookie_shortcode_popup',
				array(
					'always_visible' => true,
				)
			);

		}
	}


	/**
	 * Shortcode callback
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public function process( $atts ) {

		return $this->render();

	}

	/**
	 * Render shortcode template
	 */
	public function render() {

		$options = array_merge(
			CT_Wp_GDPR::instance()->get_controller_by_id( CT_Wp_GDPR_Controller_Cookie::ID )->get_default_options(),
			CT_Wp_GDPR::instance()->get_admin_controller()->get_options( CT_Wp_GDPR_Controller_Cookie::ID ),
			array( 'cookie_modal_always_visible' => true )
		);

		ob_start();
		ct_wp_gdpr_locate_template(
			"cookie-group-popup",
			true,
			$options
		);
		return ob_get_clean();

	}
}