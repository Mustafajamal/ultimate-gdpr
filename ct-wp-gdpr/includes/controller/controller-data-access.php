<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class CT_Wp_GDPR_Data_Access
 *
 */
class CT_Wp_GDPR_Controller_Data_Access extends CT_Wp_GDPR_Controller_Abstract {

	/**
	 *
	 */
	const ID = 'ct-wp-gdpr-dataaccess';

	/**
	 * @var
	 */
	private $services;


	/**
	 * @return string
	 */
	public function get_id() {
		return self::ID;
	}

	/**
	 * @return mixed
	 */
	private function collect_user_data() {

		$services = CT_Wp_GDPR_Model_Services::instance()->get_services( $this->options );

		/** @var CT_Wp_GDPR_Service_Abstract $service */
		foreach ( $services as $service ) {

			$this->services[] = $service->set_user( $this->user )
			                            ->collect();

		}

		return $this;

	}

	/**
	 *
	 * @return mixed
	 */
	public function render_user_data_email() {

		$rendered = '';

		/** @var CT_Wp_GDPR_Service_Abstract $service */
		foreach ( $this->services as $service ) {


			$rendered .= "" . esc_html( $service->get_name() ) . "";
			$rendered .= PHP_EOL . PHP_EOL . $service->render_collected() . PHP_EOL . PHP_EOL;

		}

		return $rendered;

	}

	/**
	 * @return array
	 */
	public function render_user_data_email_attachments() {

		$attachments = array();

		$file_combined = $this->create_attachment_file( 'all-services', 'txt' );

		/** @var CT_Wp_GDPR_Service_Abstract $service */
		foreach ( $this->services as $service ) {

			$rendered = $service->render_collected();

			if ( ! $rendered ) {
				continue;
			}

			$file = $this->create_attachment_file( $service->get_id(), 'json' );

			// pushing data to temp file created via WP
			file_put_contents( $file, $rendered );

			$combined_rendered = $service->get_name() . PHP_EOL . PHP_EOL . $rendered . PHP_EOL . PHP_EOL;

			// pushing data to temp file created via WP
			file_put_contents( $file_combined, $combined_rendered, FILE_APPEND );

			$attachments[] = $file;
		}

		$attachments[] = $file_combined;

		return $attachments;

	}

	/**
	 *
	 * Use Wordpress built in wp_tempnam to create attachment
	 *
	 * @param $name
	 * @param $extension
	 *
	 * @return mixed
	 */
	private function create_attachment_file( $name, $extension ) {

		$file     = wp_tempnam( $name );
		$file_new = str_replace( ".tmp", ".$extension", $file );
		rename( $file, $file_new );

		return $file_new;

	}

	/**
	 * @param $to
	 * @param $subject
	 * @param $message
	 * @param array $attachments
	 *
	 * @return CT_Wp_GDPR_Controller_Data_Access
	 */
	private function send_mail( $to, $subject, $message, $attachments = array() ) {

		wp_mail( $to, $subject, $message, 'Content-Type: text/html; charset=UTF-8', $attachments );

		foreach ( $attachments as $attachment ) {
			unlink( $attachment );
		}

		return $this;
	}

	/**
	 *
	 */
	public function init() {
		$this->services = array();
		add_action( 'ct_wp_gdpr_after_controllers_registered', array( $this, 'legacy_update' ) );
	}

	/**
	 *
	 */
	public function front_action() {

		if ( $this->is_user_request_for_user_data() ) {

			$this->process_request_for_user_data();

		}

	}

	/**
	 * @return bool
	 */
	private function process_request_for_user_data() {

		$email = sanitize_email( ct_wp_gdpr_get_value( 'ct-wp-gdpr-email', $this->get_request_array() ) );

		if ( ! $email ) {
			CT_Wp_GDPR_Model_Front_View::instance()->add( 'notices', esc_html__( "Wrong email!", 'ct-wp-gdpr' ) );

			return false;
		}

		if ( ! ct_wp_gdpr_recaptcha_verify() ) {
			CT_Wp_GDPR_Model_Front_View::instance()->add( 'notices', esc_html__( "Recaptcha error!", 'ct-wp-gdpr' ) );

			return false;
		}

		$user = new CT_Wp_GDPR_Model_User( array( 'user_email' => $email ) );
		$this->set_user( $user );
		$this->save_user_request();

		$notify_mail = $this->get_option( 'dataaccess_notify_mail' );
		if ( $notify_mail ) {
			wp_mail(
				$notify_mail,
				esc_html__( "[Wp GDPR] New Data Access request", 'ct-wp-gdpr' ),
				sprintf(
					esc_html__( "There is a new data access request from email %s. See details at %s", 'ct-wp-gdpr' ),
					$email,
					admin_url( 'admin.php?page=ct-wp-gdpr-dataaccess' )
				),
				'Content-Type: text/html; charset=UTF-8'
			);

		}

		CT_Wp_GDPR_Model_Front_View::instance()->add( 'notices', esc_html__( "Your request was sent!", 'ct-wp-gdpr' ) );

		return true;

	}

	/**
	 *
	 */
	private
	function is_user_request_for_user_data() {

		$email   = ct_wp_gdpr_get_value( 'ct-wp-gdpr-email', $this->get_request_array() );
		$consent = ct_wp_gdpr_get_value( 'ct-wp-gdpr-consent', $this->get_request_array() );
		$submit  = ct_wp_gdpr_get_value( 'ct-wp-gdpr-data-access-submit', $this->get_request_array() );

		if ( ! $email || ! $consent || ! $submit ) {
			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 */
	private
	function is_admin_request_for_users_send_email() {

		if (
			ct_wp_gdpr_get_value( 'ct-wp-gdpr-data-access-send', $this->get_request_array() ) &&
			ct_wp_gdpr_get_value( 'emails', $this->get_request_array() )
		) {
			return true;
		}

		return false;

	}

	/**
	 * @return bool
	 */
	private
	function is_admin_request_for_users_remove() {

		if (
			(
				ct_wp_gdpr_get_value( 'ct-wp-gdpr-data-access-remove', $this->get_request_array() ) &&
				ct_wp_gdpr_get_value( 'emails', $this->get_request_array() )
			) ||
			(
			ct_wp_gdpr_get_value( 'ct-wp-gdpr-data-access-remove-all-sent', $this->get_request_array() )
			)
		) {
			return true;
		}

		return false;

	}

	/**
	 * @return bool
	 */
	private
	function is_admin_request_for_user_data() {

		if ( ct_wp_gdpr_get_value( 'ct-wp-gdpr-data-access-details', $this->get_request_array() ) ) {
			return true;
		}

		return false;
	}

	/**
	 *
	 */
	private
	function users_send_email() {

		$emails   = ct_wp_gdpr_get_value( 'emails', $this->get_request_array() );
		$requests = get_option( $this->get_requests_option_key(), array() );

		foreach ( $emails as $email ) {

			$email = sanitize_email( $email );
			$this->set_user( new CT_Wp_GDPR_Model_User( array( 'user_email' => $email ) ) );
			$this->collect_user_data();
			$this->send_mail(
				$email,
				$this->get_option( 'dataaccess_mail_title' ),
				$this->get_option( 'dataaccess_mail_content' ),
				$this->render_user_data_email_attachments()
			);

			if ( isset( $requests[ $email ] ) && is_array( $requests[ $email ] ) ) {

				$requests[ $email ]['status'] = time();

			}

		}

		update_option( $this->get_requests_option_key(), $requests );

		$this->add_view_option(
			'notices',
			array(
				sprintf(
					esc_html__( "Data was sent to email: %s", 'ct-wp-gdpr' ),
					implode( ', ', $emails )
				)
			)
		);

	}

	/**
	 *
	 */
	private
	function users_remove_from_list() {

		$remove_all_sent = ct_wp_gdpr_get_value( 'ct-wp-gdpr-data-access-remove-all-sent', $this->get_request_array() );
		$emails          = ct_wp_gdpr_get_value( 'emails', $this->get_request_array(), array() );
		$requests        = get_option( $this->get_requests_option_key(), array() );

		foreach ( $requests as $key => $request ) {

			if (
				( ! $remove_all_sent && in_array( $key, $emails ) ) ||
				( $remove_all_sent && ! empty( $request['status'] ) )
			) {

				{
					unset( $requests[ $key ] );
				}

			}

		}

		update_option( $this->get_requests_option_key(), $requests );

		$this->add_view_option(
			'notices',
			array(
				sprintf(
					esc_html__( "The following emails were removed from the list: %s", 'ct-wp-gdpr' ),
					implode( ', ', $emails )
				)
			)
		);

	}

	/**
	 *
	 */
	private
	function user_data_preview() {

		$email = ct_wp_gdpr_get_value( 'email', $this->get_request_array() );
		$this->set_user( new CT_Wp_GDPR_Model_User( array(
			'user_email' => $email,
		) ) );

		$this->collect_user_data();

		$service_data = array();

		/** @var CT_Wp_GDPR_Service_Abstract $service */
		foreach ( $this->services as $service ) {
			$service_data[ $service->get_name() ] = $service->render_collected();
		}

		$view_options = array(
			'email' => $email,
			'data'  => $service_data,
		);
		$this->set_view_options( $view_options );

	}

	/**
	 */
	protected
	function admin_page_action() {

		if ( $this->is_admin_request_for_user_data() ) {
			$this->user_data_preview();

			// custom view
			return;

		}

		if ( $this->is_admin_request_for_users_send_email() ) {
			$this->users_send_email();
		}

		if ( $this->is_admin_request_for_users_remove() ) {
			$this->users_remove_from_list();
		}

		/* Default settings page */
		$this->add_view_option( 'data', $this->get_all_requested_users_data() );

	}

	/**
	 * @return array
	 */
	private
	function get_all_requested_users_data() {

		$data        = array();
		$requests    = get_option( $this->get_requests_option_key(), array() );
		$date_format = apply_filters( 'ct_wp_gdpr_date_format', 'd M Y H:m:s' );

		if ( ! $requests ) {
			return $data;
		}

		foreach ( $requests as $request ) {

			$user   = new CT_Wp_GDPR_Model_User( array( 'user_email' => $request['user_email'] ) );
			$datum  = array(
				'id'         => $user->get_id(),
				'user_email' => $request['user_email'],
				'date'       => date( $date_format, $request['date'] ),
				'status'     => $request['status'] ? date( $date_format, $request['date'] ) : esc_html__( "Not sent", 'ct-wp-gdpr' ),
			);
			$data[] = $datum;

		}

		// recent go first
		$data = array_reverse( $data );

		return $data;

	}

	/**
	 *
	 */
	private
	function save_user_request() {

		$requests                             = get_option( $this->get_requests_option_key(), array() );
		$requests[ $this->user->get_email() ] = array(
			'id'         => $this->user->get_id(),
			'user_email' => $this->user->get_email(),
			'date'       => time(),
			'status'     => 0,
		);
		update_option( $this->get_requests_option_key(), $requests );

	}

	/**
	 * @return mixed
	 */
	public
	function add_menu_page() {

		add_submenu_page(
			CT_Wp_GDPR::instance()->get_admin_controller()->get_option_name(),
			esc_html__( 'Data Access', 'ct-wp-gdpr' ),
			esc_html__( 'Data Access', 'ct-wp-gdpr' ),
			'manage_options',
			'ct-wp-gdpr-dataaccess',
			array( $this, 'render_menu_page' )
		);

	}

	/**
	 * @return string
	 */
	public
	function get_view_template() {

		if ( ct_wp_gdpr_get_value( 'ct-wp-gdpr-data-access-details', $this->get_request_array() ) ) {
			return 'admin/admin-data-access-user-details';
		}

		return 'admin/admin-data-access';
	}

	/**
	 * Do actions in admin (general)
	 */
	public
	function admin_action() {
	}

	/**
	 * @return mixed
	 */
	public
	function add_option_fields() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		/* Data access section */

		add_settings_section(
			'ct-wp-gdpr-dataaccess', // ID
			esc_html__( 'Data access settings', 'ct-wp-gdpr' ), // Title
			null, // callback
			'ct-wp-gdpr-dataaccess' // Page
		);

		/* Data accesss section fields */

		{
			add_settings_field(
				'dataaccess_notify_email', // ID
				esc_html__( "Email to send new request notifications to", 'ct-wp-gdpr' ), // Title
				array( $this, 'render_field_dataaccess_notify_mail' ), // Callback
				'ct-wp-gdpr-dataaccess', // Page
				'ct-wp-gdpr-dataaccess' // Section
			);

			add_settings_field(
				'dataaccess_mail_title', // ID
				esc_html__( 'Mail title', 'ct-wp-gdpr' ), // Title
				array( $this, 'render_field_dataaccess_mail_title' ), // Callback
				$this->get_id(), // Page
				$this->get_id() // Section
			);

			add_settings_field(
				'dataaccess_mail_content', // ID
				esc_html__( 'Mail content', 'ct-wp-gdpr' ), // Title
				array( $this, 'render_field_dataaccess_mail_content' ), // Callback
				$this->get_id(), // Page
				$this->get_id() // Section
			);

		}

	}

	/**
	 *
	 */
	public
	function render_field_dataaccess_mail_content() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );

		wp_editor(
			$admin->get_option_value( $field_name, '' ),
			$this->get_id() . '_' . $field_name,
			array(
				'textarea_rows' => 20,
				'textarea_name' => $admin->get_field_name_prefixed( $field_name ),
			)
		);

	}

	/**
	 *
	 */
	public
	function render_field_dataaccess_mail_title() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input type='text' id='%s' name='%s' value='%s' />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name, '' )
		);

	}

	/**
	 *
	 */
	public
	function render_field_dataaccess_notify_mail() {

		$admin      = CT_Wp_GDPR::instance()->get_admin_controller();
		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input type='text' id='%s' name='%s' value='%s' />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name, '' )
		);

	}

	/**
	 * @return array|mixed
	 */
	public
	function get_default_options() {

		return apply_filters( "ct_wp_gdpr_controller_{$this->get_id()}_default_options", array(
			'dataaccess_notify_email' => get_bloginfo( 'admin_email' ),
			'dataaccess_mail_title'   => sprintf( esc_html__( "[Wp GDPR] Data request results from %s", 'ct-wp-gdpr' ), get_bloginfo( 'name' ) ),
			'dataaccess_mail_content' => sprintf( esc_html__( "Please find data attached", 'ct-wp-gdpr' ) ),
		) );

	}

	/**
	 * @return string
	 */
	public
	function get_requests_option_key() {
		return $this->get_id() . '-requests';
	}


	/**
	 * Update options for legacy structure
	 */
	public
	function legacy_update() {

		/* Request data was moved from controller option to requests option in v1.1 */

		// get legacy requests data
		$legacy_requests = $this->get_option( 'requests', array() );

		// something to update
		if ( $legacy_requests && is_array( $legacy_requests ) ) {

			// new requests structure
			$requests = get_option( $this->get_requests_option_key(), array() );

			// merge requests
			$requests = array_merge( $legacy_requests, $requests );

			// fill new structure with data
			update_option( $this->get_requests_option_key(), $requests );

		}

		// destroy old structure data
		if ( $legacy_requests ) {

			$options = get_option( $this->get_id(), array() );
			unset( $options['requests'] );
			update_option( $this->get_id(), $options );

		}

	}

}
