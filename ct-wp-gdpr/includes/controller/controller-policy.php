<?php

/**
 * Class CT_Wp_GDPR_Controller_Policy
 */
class CT_Wp_GDPR_Controller_Policy extends CT_Wp_GDPR_Controller_Abstract {

	/**
	 *
	 */
	const ID = 'ct-wp-gdpr-policy';

	/**
	 * Add menu page (if not added in admin controller)
	 */
	public function add_menu_page() {

		add_submenu_page(
			CT_Wp_GDPR::instance()->get_admin_controller()->get_option_name(),
			esc_html__( 'Privacy Policy', 'ct-wp-gdpr' ),
			esc_html__( 'Privacy Policy', 'ct-wp-gdpr' ),
			'manage_options',
			$this->get_id(),
			array( $this, 'render_menu_page' )
		);
	}

	/**
	 * Get view template string
	 * @return string
	 */
	public function get_view_template() {
		return '/admin/admin-policy';
	}

	/**
	 * @return mixed
	 */
	public function add_option_fields() {

		/* Section */

		add_settings_section(
			$this->get_id(), // ID
			esc_html__( 'Privacy Policy', 'ct-wp-gdpr' ), // Title
			null, // callback
			$this->get_id() // Page
		);

		/* Section fields */

		add_settings_field(
			'policy_header', // ID
			esc_html__( 'Instructions', 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_policy_header' ), // Callback
			$this->get_id(), // Page
			$this->get_id() // Section
		);

		add_settings_field(
			'policy_require_users', // ID
			esc_html__( 'Require logged in users to accept Privacy Policy (redirect)', 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_policy_require_users' ), // Callback
			$this->get_id(), // Page
			$this->get_id() // Section
		);

		add_settings_field(
			'policy_require_guests', // ID
			esc_html__( 'Require not logged in users/guests to accept Privacy Policy (redirect)', 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_policy_require_guests' ), // Callback
			$this->get_id(), // Page
			$this->get_id() // Section
		);

		add_settings_field(
			'policy_wp_page', // ID
			esc_html__( 'WordPress Privacy Policy page', 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_policy_wp_page' ), // Callback
			$this->get_id(), // Page
			$this->get_id() // Section
		);

		add_settings_field(
			'policy_target_page', // ID
			esc_html__( 'Page with existing Privacy Policy', 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_policy_target_page' ), // Callback
			$this->get_id(), // Page
			$this->get_id() // Section
		);

		add_settings_field(
			'policy_target_custom',
			esc_html__( 'Privacy Policy Custom URL', 'ct-wp-gdpr' ),
			array( $this, 'render_field_policy_target_custom' ),
			$this->get_id(),
			$this->get_id()
		);

		add_settings_field(
			'policy_after_page', // ID
			esc_html__( 'Page to redirect to after Privacy Policy accepted', 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_policy_after_page' ), // Callback
			$this->get_id(), // Page
			$this->get_id() // Section
		);

		add_settings_field(
			'policy_expire', // ID
			esc_html__( 'Set consent expire time [s]', 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_policy_expire' ), // Callback
			$this->get_id(), // Page
			$this->get_id() // Section
		);

		add_settings_field(
			'policy_version', // ID
			esc_html__( 'Privacy Policy version, eg. 1.0 (if you change it, user has to give consent again)', 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_policy_version' ), // Callback
			$this->get_id(), // Page
			$this->get_id() // Section
		);

		add_settings_field(
			'policy_redirect_first', // ID
			esc_html__( 'Redirect to Privacy Policy first (if Terms and Conditions also redirect)', 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_policy_redirect_first' ), // Callback
			$this->get_id(), // Page
			$this->get_id() // Section
		);

		add_settings_field(
			'policy_placeholder', // ID
			esc_html__( "Convert the following text to Privacy Policy link in all services templates, eg. 'Privacy Policy'", 'ct-wp-gdpr' ), // Title
			array( $this, 'render_field_policy_placeholder' ), // Callback
			$this->get_id(), // Page
			$this->get_id() // Section
		);

	}

	/**
	 *
	 */
	public function render_field_policy_wp_page() {

		$url = admin_url( 'privacy.php' );

		printf(
			"<a href='%s' target='_blank'>%s</a>",
			$url,
			esc_html__( 'Click here to create' )
		);

	}

	/**
	 *
	 */
	public function render_field_policy_header() {

		printf(
			esc_html__( '1. Place %s shortcode on your existing Privacy Policy page to add an accept button.%s2. Select Privacy Policy page below, so users can be redirected there to give their consent.', 'ct-wp-gdpr' ),
			'<b>[wp_gdpr_policy_accept]</b>',
			'<br>'
		);

	}

	/**
	 * Do actions in admin (general)
	 */
	public function admin_action() {
	}

	/**
	 * Get unique controller id (page name, option id)
	 */
	public function get_id() {
		return self::ID;
	}

	/**
	 * Do actions on current admin page
	 */
	protected function admin_page_action() {

		if ( $this->is_request_consents_log() ) {
			$this->download_consents_log();
		}

	}

	/**
	 * @return bool|mixed
	 */
	private function is_request_consents_log() {
		return ct_wp_gdpr_get_value( 'ct-wp-gdpr-log', $this->get_request_array() );
	}

	/**
	 * Download logs of all user consents
	 */
	private function download_consents_log() {

		global $wpdb;

		// get all user metas
		$sql = $wpdb->prepare(
			"
				SELECT user_id, meta_value 
				FROM {$wpdb->usermeta}
				WHERE meta_key = %s
			",
			$this->get_id()
		);

		$results = $wpdb->get_results( $sql, ARRAY_A );

		// default to array
		if ( ! $results ) {
			$results = array();
		}

		// create a response
		$response = '';
		foreach ( $results as $result ) {

			$id      = $result['user_id'];
			$data    = maybe_unserialize( ( $result['meta_value'] ) );
			$expire  = $data['policy_expire_time'];
			$version = $data['policy_version'];

			// either get consent given time (v1.4) or calculate it
			$created = isset( $data['policy_consent_time'] ) ? $data['policy_consent_time'] : ( $expire - (int) $this->get_option( 'policy_expire', YEAR_IN_SECONDS ) );

			// format dates
			$expire  = ct_wp_gdpr_date( $expire );
			$created = ct_wp_gdpr_date( $created );

			$response .= sprintf(
				__( "user id: %d \r\nconsent version: %s \r\nconsent given: %s \r\nconsent expires: %s \r\n\r\n", 'ct-wp-gdpr' ),
				$id, $version, $created, $expire
			);

		}

		// download
		header( "Content-Type: application/octet-stream" );
		header( "Content-Disposition: attachment; filename='{$this->get_id()}-logs.txt'" );
		echo $response;
		exit;

	}

	/**
	 * @param $url
	 */
	public static function set_redirect_after_page( $url ) {
		$_SESSION['ct-wp-gdpr-policy-redirect-after'] = $url;
	}

	/**
	 * @return bool|mixed
	 */
	public static function get_redirect_after_page() {
		if ( ! isset( $_SESSION ) ) {
			return '';
		}

		return ct_wp_gdpr_get_value( 'ct-wp-gdpr-policy-redirect-after', $_SESSION );
	}

	/**
	 * Init after construct
	 */
	public function init() {

		add_action( 'wp_ajax_ct_wp_gdpr_policy_consent_give', array( $this, 'give_consent' ) );
		add_action( 'wp_ajax_nopriv_ct_wp_gdpr_policy_consent_give', array( $this, 'give_consent' ) );
		add_action( 'wp_ajax_ct_wp_gdpr_policy_consent_decline', array( $this, 'decline_consent' ) );
		add_action( 'wp_ajax_nopriv_ct_wp_gdpr_policy_consent_decline', array( $this, 'decline_consent' ) );

		// also for ajax forms
		$this->add_placeholders();

	}

	/**
	 * Do actions on frontend
	 */
	public function front_action() {

		$this->set_front_view();

		if ( $this->should_redirect_user() ) {
			$this->schedule_redirect();
		}

	}

	/**
	 * @return bool
	 */
	private function should_redirect_user() {

		$target_page = $this->get_target_page_id();

		if ( ! $target_page && !$this->get_option( 'policy_target_custom' ) ) {
			return false;
		}

		if ( get_post() && get_post()->ID == $target_page && !$this->get_option( 'policy_target_custom' ) ) {
			return false;
		}

		$terms_page = CT_Wp_GDPR::instance()->get_admin_controller()->get_option_value( 'terms_target_page', '', CT_Wp_GDPR_Controller_Terms::ID, 'page' );
		if ( $terms_page && get_post() && $terms_page == get_post()->ID ) {
			return false;
		}

		if ( is_user_logged_in() ) {

			if ( ! $this->get_option( 'policy_require_users' ) ) {
				return false;
			}

		} else {

			if ( ! $this->get_option( 'policy_require_guests' ) ) {
				return false;
			}

		}

		if ( $this->is_consent_valid() ) {
			return false;
		}

		return true;

	}

	/**
	 *
	 */
	private function schedule_redirect() {

		$current_url = set_url_scheme( "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
		self::set_redirect_after_page( $current_url );
		$priority = CT_Wp_GDPR_Model_Redirect::PRIORITY_STANDARD + ( $this->get_option( 'policy_redirect_first' ) ? - 1 : 1 );
		$url = $this->get_custom_target_page() ? $this->get_custom_target_page() : get_permalink( $this->get_target_page_id() );
		new CT_Wp_GDPR_Model_Redirect(
			$url,
			$priority
		);
	}

	private function get_custom_target_page() {
		$read_more_url = false;
		$policy_target_custom = $this->get_option( 'policy_target_custom' );
		if ( $policy_target_custom && ! strpos( $policy_target_custom, 'http' ) ) {
			$read_more_url = 'http://' . $policy_target_custom;
		}
		return $read_more_url;
	}

	/**
	 * @param string $variable_name
	 * @param string $variable_default_value
	 *
	 * @return array|mixed|object|string
	 */
	private function get_cookie( $variable_name = '', $variable_default_value = '' ) {

		$value = ct_wp_gdpr_get_encoded_cookie( $this->get_id() );
		$cookie = $value ? json_decode( stripslashes( $value ), true ) : array();

		if ( $variable_name ) {
			return is_array( $cookie ) && isset( $cookie[ $variable_name ] ) ? $cookie[ $variable_name ] : $variable_default_value;
		}

		return $cookie;

	}

	/**
	 * @return bool
	 */
	private function is_consent_valid() {

		$user_meta = get_user_meta( $this->user->get_current_user_id(), $this->get_id(), true );

		$time_user_valid    = false;
		$version_user_valid = false;
		if ( $this->user->get_current_user_id() ) {

			$time_user_valid = (
				is_array( $user_meta ) &&
				! empty( $user_meta['policy_expire_time'] ) &&
				$user_meta['policy_expire_time'] > time()
			);

			$version_user_valid = (
				is_array( $user_meta ) &&
				! empty( $user_meta['policy_version'] ) &&
				$user_meta['policy_version'] === $this->get_option( 'policy_version' )
			);

		}

		$cookie_date       = $this->get_cookie( 'policy_expire_time', 0 );
		$time_cookie_valid = (
			$cookie_date &&
			$cookie_date > time()
		);

		$cookie_version       = $this->get_cookie( 'policy_version' );
		$version_cookie_valid = ( $cookie_version === $this->get_option( 'policy_version' ) );

		return ( $time_user_valid || $time_cookie_valid ) && ( $version_user_valid || $version_cookie_valid );

	}

	/**
	 *
	 */
	private function set_front_view() {

		if ( $this->is_consent_valid() ) {
			CT_Wp_GDPR_Model_Front_View::instance()->set( 'policy_accepted', true );
		}

	}

	/**
	 * @param int $custom_expire_time
	 */
	public function give_consent( $custom_expire_time = 0 ) {

		$time = time();

		$expire_time = $custom_expire_time ?
			$custom_expire_time :
			$time + (int) $this->get_option( 'policy_expire', YEAR_IN_SECONDS );

		$data = array(
			'policy_consent_time' => $time,
			'policy_expire_time'  => $expire_time,
			'policy_version'      => $this->get_option( 'policy_version' ),
		);

		ct_wp_gdpr_set_encoded_cookie(
			$this->get_id(),
			json_encode( $data, JSON_UNESCAPED_SLASHES ),
			$expire_time,
			'/'
		);

		if ( is_user_logged_in() ) {
			update_user_meta( $this->user->get_current_user_id(), $this->get_id(), $data );
		}

	}

	/**
	 *
	 */
	public function decline_consent() {

		setcookie( $this->get_id(), '', 1, '/' );

		if ( is_user_logged_in() ) {
			delete_user_meta( $this->user->get_current_user_id(), $this->get_id() );
		}

	}

	/**
	 *
	 */
	public function render_field_policy_content() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );

		wp_editor(
			$admin->get_option_value( $field_name, '' ),
			$this->get_id() . '_' . $field_name,
			array(
				'textarea_rows' => 20,
				'textarea_name' => $admin->get_field_name_prefixed( $field_name ),
			)
		);

	}

	/**
	 *
	 */
	public function render_field_policy_placeholder() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input type='text' id='%s' name='%s' value='%s' />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name )
		);

	}

	/**
	 *
	 */
	public function render_field_policy_version() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input type='text' id='%s' name='%s' value='%s' />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name )
		);

	}

	/**
	 *
	 */
	public function render_field_policy_expire() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input type='text' id='%s' name='%s' value='%s' />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name )
		);

	}

	/**
	 *
	 */
	public function render_field_policy_target_page() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		$value      = $admin->get_option_value( $field_name );
		$post_types = ct_wp_gpdr_get_default_post_types();
		$posts      = ct_wp_gdpr_wpml_get_original_posts( array(
			'posts_per_page' => - 1,
			'post_type'      => $post_types,
		) );

		printf(
			'<select class="ct-wp-gdpr-field" id="%s" name="%s">',
			$field_name,
			$admin->get_field_name_prefixed( $field_name )
		);

		// empty option
		echo "<option></option>";

		// wordpress default policy page option
		$wp_page             = new stdClass();
		$wp_page->ID         = 'wp';
		$wp_page->post_title = esc_html__( 'WordPress Privacy page', 'ct_theme' );
		array_unshift( $posts, $wp_page );

		/** @var WP_Post $post */
		foreach ( $posts as $post ) :

			$post_title = $post->post_title ? $post->post_title : $post->post_name;
			$post_id    = $post->ID;
			$selected   = $post_id == $value ? "selected" : '';
			echo "<option value='$post->ID' $selected>$post_title</option>";

		endforeach;

		echo '</select>';

	}

	public function render_field_policy_target_custom() {
		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		$value      = $admin->get_option_value( $field_name );

		printf(
			"<input type='text' id='%s' name='%s' value='%s' />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			esc_html( $value )
		);
	}

	/**
	 *
	 */
	public function render_field_policy_after_page() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		$value      = $admin->get_option_value( $field_name );
		$post_types = ct_wp_gpdr_get_default_post_types();
		$posts      = ct_wp_gdpr_wpml_get_original_posts( array(
			'posts_per_page' => - 1,
			'post_type'      => $post_types,
		) );

		printf(
			'<select class="ct-wp-gdpr-field" id="%s" name="%s">',
			$field_name,
			$admin->get_field_name_prefixed( $field_name )
		);

		// add default options
		printf( "<option value='-1'>%s</option>", esc_html__( 'Last visited page', 'ct-wp-gdpr' ) );
		printf( "<option value='0'>%s</option>", esc_html__( "Don't redirect", 'ct-wp-gdpr' ) );

		/** @var WP_Post $post */
		foreach ( $posts as $post ) :

			$post_title = $post->post_title ? $post->post_title : $post->post_name;
			$post_id    = $post->ID;
			$selected   = $post_id == $value ? "selected" : '';
			echo "<option value='$post->ID' $selected>$post_title</option>";

		endforeach;

		echo '</select>';

	}

	/**
	 *
	 */
	public function render_field_policy_redirect_first() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input class='ct-wp-gdpr-field' type='checkbox' id='%s' name='%s' %s />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name ) ? 'checked' : ''
		);

	}

	/**
	 *
	 */
	public function render_field_policy_require_users() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input class='ct-wp-gdpr-field' type='checkbox' id='%s' name='%s' %s />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name ) ? 'checked' : ''
		);

	}

	/**
	 *
	 */
	public function render_field_policy_require_guests() {

		$admin = CT_Wp_GDPR::instance()->get_admin_controller();

		$field_name = $admin->get_field_name( __FUNCTION__ );
		printf(
			"<input class='ct-wp-gdpr-field' type='checkbox' id='%s' name='%s' %s />",
			$admin->get_field_name( __FUNCTION__ ),
			$admin->get_field_name_prefixed( $field_name ),
			$admin->get_option_value_escaped( $field_name ) ? 'checked' : ''
		);

	}

	/**
	 * @return array|
	 */
	public function get_default_options() {

		return apply_filters( "ct_wp_gdpr_controller_{$this->get_id()}_default_options", array(
			'forgotten_notify_mail' => get_bloginfo( 'admin_email' ),
			'policy_expire'         => YEAR_IN_SECONDS,
			'policy_version'        => '1.0',
			'policy_placeholder'    => esc_html__( 'Privacy Policy', 'ct-wp-gdpr' )
		) );

	}

	/**
	 * Set a placeholder for templates
	 */
	private function add_placeholders() {
		$url = $this->get_custom_target_page() ? $this->get_custom_target_page() : get_permalink( $this->get_target_page_id() );
		CT_Wp_GDPR_Model_Placeholders::instance()->add(
			$this->get_option( 'policy_placeholder' ),
			sprintf(
				'<a href="%s">%s</a>',
				$url,
				$this->get_option( 'policy_placeholder' )
			)
		);
	}

	/**
	 * @return int
	 */
	private function get_target_page_id() {

		$page_id = $this->get_option( 'policy_target_page', 0 );

		if ( $page_id === 'wp' ) {
			$page_id = (int) get_option( 'wp_page_for_privacy_policy' );
		}

		$page_id = ct_wp_gdpr_wpml_translate_id( $page_id, 'page' );

		return $page_id;
	}

}