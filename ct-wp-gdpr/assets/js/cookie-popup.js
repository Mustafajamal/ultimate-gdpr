/**
 * cookie popup features
 * @var object ct_wp_gdpr_cookie - from wp_localize_script
 * @var object ct_wp_gdpr_cookie_shortcode_popup - from wp_localize_script
 *
 * */
jQuery(document).ready(function ($) {

    function isModalAlwaysVisible() {
        return !! ( window.ct_wp_gdpr_cookie_shortcode_popup && ct_wp_gdpr_cookie_shortcode_popup.always_visible );
    }

    function hidePopup() {

        if ( isModalAlwaysVisible() ) return;

        jQuery('#ct-wp-gdpr-cookie-popup').hide();
        jQuery('#ct-wp-gdpr-cookie-open').show();
    }

    function showPopup() {
        jQuery('#ct-wp-gdpr-cookie-popup').show();
    }

    function hideModal() {

        if ( isModalAlwaysVisible() ) return;

        jQuery('#ct-wp-gdpr-cookie-modal').hide();
        jQuery('#ct-wp-gdpr-cookie-open').show();
    }

    function showModal() {
        jQuery('#ct-wp-gdpr-cookie-modal').show();
        jQuery('#ct-wp-gdpr-cookie-open').hide();
    }

    // hide popup and show small gear icon if user already given consent
    if (ct_wp_gdpr_cookie.consent) {
        hidePopup();
    } else {
        showPopup();
        $('body').removeClass("ct-wp-gdpr-cookie-bottomPanel-padding");
        $('body').removeClass("ct-wp-gdpr-cookie-topPanel-padding");
    }

    function setTempCookie() {
        if (!document.cookie || -1 === document.cookie.indexOf('ct-wp-gdpr-cookie')) {
            document.cookie = "ct-wp-gdpr-cookie=eyJjb25zZW50X2RlY2xpbmVkIjpmYWxzZSwiY29uc2VudF9leHBpcmVfdGltZSI6MTU1NzkzNTcyMCwiY29uc2VudF9sZXZlbCI6Mn0=; expires=Thu, 18 Dec 2050 12:00:00 UTC; path=/";
        }
    }

    function onAccept() {
        jQuery.post(ct_wp_gdpr_cookie.ajaxurl, {
                "action": "ct_wp_gdpr_cookie_consent_give",
                "level": 5 /* maximum level */
            },
            function () {
                if (ct_wp_gdpr_cookie.reload) {
                    window.location.reload(true);
                }
            }
        );

        if (!ct_wp_gdpr_cookie.reload) {
            hidePopup()
        }
        $('body').removeClass("ct-wp-gdpr-cookie-bottomPanel-padding");
        $('body').removeClass("ct-wp-gdpr-cookie-topPanel-padding");

        setTempCookie();

    }

    function onRead() {
        if (ct_wp_gdpr_cookie && ct_wp_gdpr_cookie.readurl) {
            window.location.href = ct_wp_gdpr_cookie.readurl;
        }
    }

    function onSave() {
        jQuery.post(ct_wp_gdpr_cookie.ajaxurl, {
            "action": "ct_wp_gdpr_cookie_consent_give",
            "level": $('.ct-wp-gdpr-cookie-modal-slider-item--active input').val()
        }, function () {
            if (ct_wp_gdpr_cookie.reload) {
                window.location.reload(true);
            }
        });

        if (!ct_wp_gdpr_cookie.reload) {
            hideModal();
            hidePopup();
        }
        $('body').removeClass("ct-wp-gdpr-cookie-bottomPanel-padding");
        $('body').removeClass("ct-wp-gdpr-cookie-topPanel-padding");
    }

    $('#ct-wp-gdpr-cookie-accept').bind('click', onAccept);
    $('#ct-wp-gdpr-cookie-read-more').bind('click', onRead);
    $('.ct-wp-gdpr-cookie-modal-btn.save').bind('click', onSave);

    //MODAL
    $('#ct-wp-gdpr-cookie-open,#ct-wp-gdpr-cookie-change-settings').on('click', function (e) {
        var modalbody = $("body");
        var modal = $("#ct-wp-gdpr-cookie-modal");
        modal.show();
        modalbody.addClass("cookie-modal-open");
        $("html").addClass("cookie-modal-open");
        e.stopPropagation();
    });

    //Close modal on x button
    $('#ct-wp-gdpr-cookie-modal-close').on('click', function () {

        if ( isModalAlwaysVisible() ) return;

        var modalbody = $("body");
        var modal = $("#ct-wp-gdpr-cookie-modal");
        modal.hide();
        modalbody.removeClass("cookie-modal-open");
        $("html").removeClass("cookie-modal-open");
    });

    //Close modal when clicking outside of modal area.
    $(document).on("click", function (e) {

        if ( isModalAlwaysVisible() ) return;

        if (!($(e.target).closest('#ct-wp-gdpr-cookie-change-settings, .ct-wp-gdpr-cookie-modal-content').length)) {
            var modalbody = $("body");
            var modal = $("#ct-wp-gdpr-cookie-modal");
            modal.hide();
            modalbody.removeClass("cookie-modal-open");
            $("html").removeClass("cookie-modal-open");
        }

        e.stopPropagation();
    });

    //SVG
    jQuery('img.ct-svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, else we gonna set it if we can.
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');
    });


    $(window).on('load', function () {

        var selected = $('.ct-wp-gdpr-cookie-modal-slider-item--active');
        var checked = selected.find('input[name=radio-group]');
        var input_id = checked.attr('id');

        selected.find('path').css('fill', '#82aa3b');
        selected.prevUntil('#ct-wp-gdpr-cookie-modal-slider-item-block').addClass('ct-wp-gdpr-cookie-modal-slider-item--selected');
        checked.parent().prevUntil('#ct-wp-gdpr-cookie-modal-slider-item-block').find('path').css('fill', '#82aa3b');

        $('#ct-wp-gdpr-cookie-modal-slider-form').attr('class', 'ct-slider-' + input_id);
        $('.ct-wp-gdpr-cookie-modal-slider-info.' + input_id).css('display', 'block');

    });

    $('.ct-wp-gdpr-cookie-modal-slider').each(function () {

        var $btns = $('.ct-wp-gdpr-cookie-modal-slider-item').click(function () {

            var $input = $(this).find('input').attr('id');

            var $el = $('.' + $input).show();
            var form_class = $('#ct-wp-gdpr-cookie-modal-slider-form');
            var modalBody = $('div#ct-wp-gdpr-cookie-modal-body');

            form_class.attr('class', 'ct-slider-' + $input);
            $('.ct-wp-gdpr-cookie-modal-slider-wrap > div').not($el).hide();

            $btns.removeClass('ct-wp-gdpr-cookie-modal-slider-item--active');
            $(this).addClass('ct-wp-gdpr-cookie-modal-slider-item--active');

            $(this).prevUntil('#ct-wp-gdpr-cookie-modal-slider-item-block').find('path').css('fill', '#82aa3b');
            $(this).prevUntil('#ct-wp-gdpr-cookie-modal-slider-item-block').addClass('ct-wp-gdpr-cookie-modal-slider-item--selected');
            $(this).find('path').css('fill', '#82aa3b');
            $(this).nextAll().find('path').css('fill', '#595959');
            $(this).removeClass('ct-wp-gdpr-cookie-modal-slider-item--selected');
            $(this).nextAll().removeClass('ct-wp-gdpr-cookie-modal-slider-item--selected');

            if ($(this).attr('id') === 'ct-wp-gdpr-cookie-modal-slider-item-block') {
                modalBody.addClass('ct-wp-gdpr-slider-block');
                modalBody.removeClass('ct-wp-gdpr-slider-not-block');
            } else {
                modalBody.removeClass('ct-wp-gdpr-slider-block');
                modalBody.addClass('ct-wp-gdpr-slider-not-block');
            }

        });

    });

    if ($("#ct-wp-gdpr-cookie-popup").hasClass("ct-wp-gdpr-cookie-topPanel")) {
        if (!ct_wp_gdpr_cookie.consent) {
            $('body').addClass("ct-wp-gdpr-cookie-topPanel-padding");
        }
    }

    if ($("#ct-wp-gdpr-cookie-popup").hasClass("ct-wp-gdpr-cookie-bottomPanel")) {
        if (!ct_wp_gdpr_cookie.consent) {
            $('body').addClass("ct-wp-gdpr-cookie-bottomPanel-padding");
        }
    }

    if ($("#ct-wp-gdpr-cookie-popup").hasClass("ct-wp-gdpr-cookie-topPanel ct-wp-gdpr-cookie-popup-modern")) {
        $('body').addClass("popup-modern-style");
    }

    if ($("#ct-wp-gdpr-cookie-popup").hasClass("ct-wp-gdpr-cookie-bottomPanel ct-wp-gdpr-cookie-popup-modern")) {
        $('body').addClass("popup-modern-style");
    }

});
