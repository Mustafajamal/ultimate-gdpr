/* terms accept shortcode features */
jQuery(document).ready(function ($) {

    function onAccept() {
        jQuery.post(ct_wp_gdpr_terms.ajaxurl, {"action": "ct_wp_gdpr_terms_consent_give"}, function () {
            ct_wp_gdpr_terms.redirect ? window.location.replace(ct_wp_gdpr_terms.redirect) : window.location.reload(true);
        });
        jQuery('#ct-wp-gdpr-terms-accept').hide();
    }

    function onDecline() {
        jQuery.post(ct_wp_gdpr_terms.ajaxurl, {"action": "ct_wp_gdpr_terms_consent_decline"}, function () {
            window.location.reload(true);
        });
        jQuery('#ct-wp-gdpr-terms-decline').hide();
    }

    $('#ct-wp-gdpr-terms-accept').bind('click', onAccept);
    $('#ct-wp-gdpr-terms-decline').bind('click', onDecline);

});