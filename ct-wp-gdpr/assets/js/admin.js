jQuery(document).ready(function ($) {

    // set color picker options
    $().wpColorPicker && $('.ct-color-field').wpColorPicker();

    // select all checkboxes
    $("input[name=services-select-all]").on('click', function () {
        if ($(this).prop('checked')) {
            $(this).parent().parent().find("input[type=checkbox]").prop('checked', true);
        } else {
            $(this).parent().parent().find("input[type=checkbox]").prop('checked', false);
        }
    });

    // hide nag notices
    $("#wpbody-content > div.updated:not(.ct-wp-gdpr), #wpbody-content > div.notice:not(.ct-wp-gdpr), #wpbody-content > div.error:not(.ct-wp-gdpr)").hide();

    // ajax buttons
    $("input[name=ct-wp-gdpr-cookie-content-language]").on('click', function () {
        $.post(
            ajaxurl,
            {
                'action': 'ct_wp_gdpr_cookie_get_option_text',
                'language': $('#cookie_content_language').val()
            },
            function (response) {
                if (!response) return;
                for( var key in response) {
                    var val = response[ key ];

                    // switch text inputs values to chosen language content
                    $('#' + key).val(val);

                    // mce editors
                    $('#ct-wp-gdpr-cookie_' + key).val(val);
                    $('#ct-wp-gdpr-cookie_' + key + '_ifr').contents().find('body').html(val);

                };
            }
        );
    });

    $('.ct-iconpicker .ct-ip-holder .ct-ip-icon').on('click', function(){
        var iconPickerPopup = $('.ct-iconpicker .ct-ip-popup');

        iconPickerPopup.slideToggle();
    });
    $('.ct-iconpicker .ct-ip-popup').on('click', 'a', function (e) {
        e.preventDefault();
        var iconClass = $(this).data('icon'),
            inputField = $('.ct-icon-value'),
            iconHolder = $('.ct-ip-icon i');
        iconHolder.attr('class', '');
        iconHolder.addClass(iconClass);
        inputField.val(iconClass);
    });

    $('.ct-iconpicker .ct-ip-popup').on('change keyup paste', 'input.ct-ip-search-input', function (e) {
        var iconPickerPopup = $('.ct-iconpicker .ct-ip-popup ul'),
            searchVal = $(this).val();

        if( _.isEmpty(searchVal) ){
            iconPickerPopup.find('li').removeClass('hidden');
        } else {
            iconPickerPopup.find('li').addClass('hidden');

            var found = iconPickerPopup.find('li a[data-icon*="'+searchVal+'"]');
            found.parent('li').removeClass('hidden');
        }


    });

    $(document).mouseup(function (e){
        var iconPicker = $('.ct-iconpicker'),
            iconPickerPopup = $('.ct-iconpicker .ct-ip-popup');

        if ( ( ! iconPicker.is(e.target) && iconPicker.has(e.target).length === 0 ) ){
            iconPickerPopup.slideUp();
        }
    });

});
