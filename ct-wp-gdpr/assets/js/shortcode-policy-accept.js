/**
 * policy accept shortcode features
 * @var object ct_wp_gdpr_policy - from wp_localize_script
 *
 * */
jQuery(document).ready(function ($) {

    function onAccept() {
        jQuery.post(ct_wp_gdpr_policy.ajaxurl, {"action": "ct_wp_gdpr_policy_consent_give"}, function () {
            ct_wp_gdpr_policy.redirect ? window.location.replace(ct_wp_gdpr_policy.redirect) : window.location.reload(true);
        });
        jQuery('#ct-wp-gdpr-policy-accept').hide();
    }

    function onDecline() {
        jQuery.post(ct_wp_gdpr_policy.ajaxurl, {"action": "ct_wp_gdpr_policy_consent_decline"}, function () {
            window.location.reload(true);
        });
        jQuery('#ct-wp-gdpr-policy-accept').hide();
    }

    $('#ct-wp-gdpr-policy-accept').bind('click', onAccept);
    $('#ct-wp-gdpr-policy-decline').bind('click', onDecline);

});