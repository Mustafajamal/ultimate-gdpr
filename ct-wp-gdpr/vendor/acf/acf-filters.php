<?php
/**
 * Created by PhpStorm.
 * User: CreateIT
 * Date: 5/30/2018
 * Time: 11:54 AM
 */

if( class_exists( 'acf_pro' ) ) {
	return;
}

// 1. customize ACF path
add_filter('acf/settings/path', 'my_acf_settings_path');

function my_acf_settings_path( $path ) {

	// update path
	$path = plugin_dir_path(__FILE__) . 'advanced-custom-fields/acf.php';

	// return
	return $path;

}


// 2. customize ACF dir
add_filter('acf/settings/dir', 'my_acf_settings_dir');

function my_acf_settings_dir( $dir ) {

	// update path
	$dir = plugin_dir_path(__FILE__) . 'advanced-custom-fields/';

	// return
	return $dir;

}

// 3. Hide ACF field group menu item
add_filter('acf/settings/show_admin', '__return_false');


// 4. Include ACF
include_once( plugin_dir_path(__FILE__) . '/advanced-custom-fields/acf.php' );

add_action("init","ct_acf_hooks_modification");
function ct_acf_hooks_modification(){
	add_filter('acf/settings/path', 'my_acf_settings_path');
	add_filter('acf/settings/dir', 'my_acf_settings_dir');
}